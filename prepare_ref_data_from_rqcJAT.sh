#!/bin/sh
usage()
{
	echo "usage: prepare_ref_data.sh JAT-KEY_LIST REF_DIR [DST_DIR] [STAR_PATH] 

	# required inputs
	#### JAT-KEY_LIST: a (potentially comma-separated list of) JAT key(s) corresponding to annotation (gff3) files in the RQC reference database
	#### REF_DIR: the name of a writeable output directory for reference prep files
	* a directory will be created within the supplied REF_DIR for the specified JAT key (list)

	# optional inputs
	#### DST_DIR: directory of dropseq-tools scripts; default $(dirname $0)/drop-seq_v2.3
	#### STAR_PATH: path to a STAR aligner executable; default 'shifter --image=docker:mgibio/star STAR'
	
	# actions
	#### find gff3 files associated with JAT keys
	#### retrieve taxonomy data from RQC
	#### find associated genome fasta files
	#### create a genome-prep config file 
	#### create a genome-prep python script from config file
	#### print to screen a command to run the prep script on cori

	# dependencies
	#### note that this requires the tool gtftogenepred; install via conda with:
	##########   conda install -c bioconda ucsc-gtftogenepred

	
"
}

if [ -z $1 ]; then usage; exit 1;  fi


#########
## GET CL PARAMS
#########
JATKEY_LIST=$1
JATKEY_STRING=$(echo $JATKEY_LIST | tr ',' '_')
JATKEY_LIST=$(echo $JATKEY_LIST | tr ',' '\n')
REFPREFIX_LIST=()
REF_DIR=$2
DST_DIR=${3:-"$(dirname $0)/drop-seq_v2.3"}
STAR=${4:-"shifter --image=docker:mgibio/star STAR"}
starVersion=$($STAR --version | grep -o [0-9].[0-9] )


#########
## SET UP OUTPUT DIRS AND FILES
##########
genomePrepConfig=scRNA_ref_prep.${JATKEY_STRING}.config
genomePrepScript=scRNA_ref_prep.${JATKEY_STRING}.sh


#####################
### MAKE CONFIG
####################

echo -e "#ID\t#Genome_full_path\tGff3_full_path" > $genomePrepConfig
for jatkey in ${JATKEY_LIST[*]}; do
	echo "finding annotation file associated with $jatkey"
	jatinfo=$(jamo report select metadata.reference_type,metadata.reference_name,file_path,file_name,metadata.parent_refs where metadata.jat_key = $jatkey as csv | tail -n +2)
	
	# confirm its an annotation file
	if [ ! "$(echo $jatinfo | awk -F ',' '{print $1}')" == "Annotation" ]; then echo "JAT KEY $jatkey is not a valid annotation file in RQC, type is $(echo $jatinfo | awk -F ',' '{print $1}')"; exit 1; fi
	
	# get taxonomy info
	refName=$(echo $jatinfo | awk -F ',' '{print $2}')
	genus=$(echo $refName| awk '{print $1}');
	spcs=$(echo $refName | awk '{print $2}'); 
	refPrefix=$(echo ${genus:0:1}${spcs:0:1})
	REFPREFIX_LIST+=( "$refPrefix" )
	
	# get annotation and genome file paths
	gff3File=$(echo $jatinfo | awk -F ',' '{print $3"/"$4}')
	genomejat=$(echo $jatinfo | awk -F ',' '{print $5}'| cut -f2 -d"'")
    echo "finding genome file associated with $jatkey"
	genomeFile=$(jamo report select file_path,file_name where metadata.jat_key = $genomejat | awk '{print $1"/"$2}')
	
	# write paths to config
	echo -e "$refPrefix"'\t'"$genomeFile"'\t'"$gff3File" >> $genomePrepConfig
done

echo "ref genome prep config file written to $genomePrepConfig"

#############
### MAKE SCRIPT
############
$(dirname $0)/genomePrep.py $genomePrepConfig -s "$STAR" -d "$DST_DIR" > /dev/null  
echo "
mv STAR STAR$starVersion
" >> prep.sh
mv prep.sh $genomePrepScript
chmod +x $genomePrepScript


#################
## print command to run on cori
#################
REFPREFIX_STRING=$(echo ${REFPREFIX_LIST[*]} | tr ' ' '-')
outdir=$REF_DIR/${JATKEY_STRING}_${REFPREFIX_STRING}
if [ ! -e $outdir ]; then 
	mkdir -p $outdir; echo "created $outdir"; 
fi
mv $genomePrepScript $outdir
mv $genomePrepConfig $outdir
genomePrepScript=${outdir}"/"${genomePrepScript}


echo "
# prep genome by running
"
echo "
sbatch --chdir $outdir \
-J prep_genomes.$JATKEY_STRING -q genepool_shared -A gtrnd \
-t 2:00:00 -N 1 -n 1 -c 6 \
-o $(echo $genomePrepScript | sed 's/.sh/.out/') \
-e $(echo $genomePrepScript | sed 's/.sh/.err/') \
$genomePrepScript 
"


