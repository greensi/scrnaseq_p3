#!/bin/bash

################
## get stats from completed run
###############
scriptsDir=`dirname "$0"`
config=$1
getLib=${2:-"."}

. $(dirname $config)/params.txt

headers=$(echo "pool	library	spcPrefix	protocol	readPairs	pct_mappedUnique	pct_genic	min_UMIsPerCell	STAMPs	mean_UMIsPerCell	totalUMIs	readsperUMI" | tr '\t' ',')
cdir=$PWD

for libID in $( echo $libIDs | tr ',' '\n' | grep "$getLib") ; do
	cd $OUT_DIR/$libID
	echo $headers > ${libID}.stats 
	readPairs=$(cat trim.report.txt | grep "Input:" | head -1 | awk '{print $2/2}')
	pct_mappedUnique=$(cat align.Log.final.out | grep "Uniquely mapped reads %"  | cut -f2 | sed 's/%//' | awk '{print $1/100}')
	pct_genic=$(cat tag.geneReport.txt | grep  ^[0-9] | awk '{ct[NR]=$1} END{print ct[2]/ct[1]}' )
	min_UMIsPerCell=$MIN_UMIS_PER_CELL
	STAMPs=$(cat dge.summary.txt | grep -v "#"  | grep -v "_" | grep "^[CATG]" | wc -l)
	totalUMIs=$(cat dge.summary.txt | grep -v "#"  | tail -n +3 | awk '{total=total+$3} END{print total}')
	mean_UMIsPerCell=$(($totalUMIs / $STAMPs))
	readsperUMI=$(cat dge.summary.txt | grep -v "#" | tail -n +3 | awk '{total=total+$3;rd=rd+$2} END{print rd/total}')

	echo $POOL_ID,$libID,$refPrefix,$PROTOCOL,$readPairs,$pct_mappedUnique,$pct_genic,$min_UMIsPerCell,$STAMPs,$mean_UMIsPerCell,$totalUMIs,$readsperUMI >> ${libID}.stats 

	echo "stats for $libID written to 
	${libID}.stats"

done
 
cd $cdir

