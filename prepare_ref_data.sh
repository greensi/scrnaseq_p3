#!/bin/bash

## reads in a single ref species name/prefix/ncbi taxonomy ID
## creates a new directory in specified refDir
## tries to find matching entry in phytozome; else downloads genome data from ncbi
## writes script to apporpriate prepare ref db files for STAR
## prints command to run the script on cori

## usage
usage="usage: $0 <refName> <refPrefix> <taxid> <(optional) genome fasta> <(optional) genome gff>"
if [ -z $1 ]; then echo $usage; exit 1; fi

refName=$1
refPrefix=$2
taxid=$3
fasta=$4
gff=$5
refDir="$SCRATCH/refData"
DST_DIR="$(dirname $0)/drop-seq_v2.3"
STAR="shifter --image=docker:mgibio/star STAR"
starVersion=$($STAR --version | grep -o [0-9].[0-9] )


## make new dir if necessary
if [ ! -e $refDir/$refName ]; then 
	mkdir $refDir/$refName
fi	

## if files are not supplied:
if [ -z $fasta ]; then

## check if genome is in phytozome
if [ -e /global/dna/dm_archive/plant/phytozome/$refName ]; then
	gff=$(ls -lt /global/dna/dm_archive/plant/phytozome/$refName/*/annotation/*.gene_exons.gff3.gz | head -1 | awk '{print $NF}')
	fasta=$(ls -lt /global/dna/dm_archive/plant/phytozome/$refName/*/assembly/*.fa.gz | head -1 | awk '{print $NF}')
else
	## otherwise, download from ncbi
	accession=$($datasets summary genome taxon $taxid | $jq '.assemblies[].assembly.assembly_accession' -r | sort | uniq | grep GCF)
	contains_gff=$($datasets summary genome accession $accession | $jq '.assemblies[].assembly.annotation_metadata.file[].type' -r | grep GENOME_GFF )
	if [ $contains_gff ]; then 
		fasta=$refDir/$refName/${refPrefix}.fasta
		gff=$refDir/$refName/${refPrefix}.gff3
		$datasets download genome accession $accession
		unzip -q ncbi_dataset.zip -d $refDir/$refName
		cat $refDir/$refName/ncbi_dataset/data/*/[cu]*.fna > $fasta
		cat $refDir/$refName/ncbi_dataset/data/*/*.gff > $gff
	else
		echo "no GFF found for accession $accession"
		exit 1
	fi
fi
fi

## set up ref genome prep config
	genomePrepConfig=$refDir/$refName/scRNA_ref_prep.config
	echo -e "#ID\t#Genome_full_path\tGff3_full_path" > $genomePrepConfig
	echo -e "$refPrefix"'\t'"$fasta"'\t'"$gff" >> $genomePrepConfig

	## set up genome prep script
	genomePrepScript=$refDir/$refName/scRNA_ref_prep.sh
	$(dirname $0)/genomePrep.py $genomePrepConfig -s "$STAR" -d "$DST_DIR" > /dev/null  
	echo -e '\n'"$(dirname $0)/reformat_gtf.sh ${refPrefix}.gtf" >> prep.sh
	echo -e '\n'"mv STAR STAR$starVersion"'\n' >> prep.sh
	mv prep.sh $genomePrepScript
	chmod +x $genomePrepScript

	## print run command
	echo "# prep genome by running"
	echo "sbatch --chdir $refDir/$refName \
	-C haswell \
	-J scRNA_ref_prep.$refName -q genepool_shared -A gtrnd \
	-t 2:00:00 -N 1 -n 1 -c 16 \
	-o $(echo $genomePrepScript | sed 's/.sh/.out/') \
	-e $(echo $genomePrepScript | sed 's/.sh/.err/') \
	$genomePrepScript 
	" | sed 's/\t//g'