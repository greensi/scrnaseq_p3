#!/bin/bash 

#######
## script to read in .gtf file and make a table associating gene names (as used by dropseqtools for DGE table headers) with geneIDs, species, and chromosomes
usage()
{
	echo "usage: make_gene_map_from_gtf.sh <gtfFile>"
}
if [ -z $1 ]; then usage; exit 1;  fi
if [ ! -e $1 ]; then usage; echo "$1 does not exist; must be a valid gtf file";  exit 1;  fi


gtf=$1
genemap=${2:-$(echo $gtf | sed 's/.gtf/.geneMap.csv/')}

echo "species,chrom,geneID,geneName" > $genemap
cat $gtf | grep "gene_id" | cut -f1,9 | awk '{
	split($1,chr,"_");
	gID="";gN=""; 
	for(ii=1;ii<=NF;ii++){
		if ($ii=="gene_name"){gN=$(ii+1)}; 
		if($ii=="gene_id"){gID=$(ii+1)}
	};
	if(length(chr)>2){
		for(ii=3;ii<=length(chr);ii++){chr[2]=chr[2]"_"chr[ii]}
	}
	gsub("_","-",gN)
	print chr[1]","chr[2]","gID","gN
}' | sed 's/\"//g' | sed 's/;//g'  | sort | uniq \
>> $genemap
