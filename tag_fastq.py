#tag_fastq.py
import os, sys, getopt
from my_baseqDrops_slim.main import tag_fastq

### get command line arguments
fullCmdArguments = sys.argv
argumentList = fullCmdArguments[1:]
unixOptions = "p:f:f2:o:r:"  
gnuOptions = ["protocol=","fastq=","fastq2=","output=","reads="]  
try:  
    arguments, values = getopt.getopt(argumentList, unixOptions, gnuOptions)
except getopt.error as err:  
    # output error, and return with an error code
    print (str(err))
    sys.exit(2)

fq2=None
mill_reads=1000
for arg, val in arguments:  
    if arg in ("-p", "--protocol"):
        protocol=val
    elif arg in ("-f", "--fastq"):
        fq1=val
    elif arg in ("-f2", "--fastq2"):
        fq2=val
    elif arg in ("-r", "--reads"):
       mill_reads=int(val)
    elif arg in ("-o", "--output"):
    	output=val

tag_fastq(protocol, fq1, fq2, output,mill_reads)