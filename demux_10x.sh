#!/bin/bash -l

pool=$1
samples=$2

sampCol=$(head -1 $samples | tr '\t' '\n' | grep -n "Sample name" | cut -f1 -d":")
bcCol=$(head -1 $samples | tr '\t' '\n' | grep -n "10x sample index oligos" | cut -f1 -d":")

## SET UP DIRS
rootDir=$SCRATCH/seqData/$pool
if [ ! -e $rootDir ]; then mkdir $rootDir; fi
cd $rootDir
samps=$(tail -n +2 $samples | cut -f$sampCol | sed 's/ /_/g')
for samp in $samps; do if [ ! -e $samp ] ; then mkdir $samp; fi; done

## LINK FASTQ
fastq=$(jamo report \select metadata.library_name,file_name where metadata.library_name = UNKNOWN,metadata.parent_library_name = $pool,metadata.fastq_type = sdm_normal | cut -f2)
jamo link filename $fastq 
#ls -lh $rootDir/$fastq

## DEMUX
 tail -n +2 $samples | cut -f $bcCol | sed 's/\"//g' | tr ',' '\n' | grep -v "^$" | awk '{print $1"+TCTTTCCC"}' > barcodes.txt
 shifter --image=docker:bryce911/bbtools:latest demuxbyname2.sh \
 in=$fastq \
 int=t overwrite=t \
 out=%.fastq.gz \
 prefixmode=f \
 delimiter=: column=10 \
 names=barcodes.txt

##COMPILE BCs FOR SAMPLES
for samp in $samps; do if [ -e $samp/$samp.fastq.gz ] ; then rm $samp/$samp.fastq.gz; fi; done
for bc in $(cat barcodes.txt | cut -f1 -d'+'); do
	samp=$(cat $samples | grep $bc"," | cut -f$sampCol | sed 's/ /_/g')
	cat ${bc}+TCTTTCCC.fastq.gz >> $samp/$samp.fastq.gz
done
