import os, sys
sys.path.append(os.getcwd()) 
import pandas as pd
import numpy as np
from time import time, ctime
from itertools import product, chain
import subprocess
import pysam, itertools, csv
from multiprocessing import  Pool
from collections import Counter
from .helper import read_file_by_lines, extract_barcode, extract_UMI, mutate_single_base, read_whitelist, check_whitelist, list_mm, dedup_mm, sum_mm


def tag_fastq(protocol, fq1file, fq2file=None, output="/dev/stdout",top_million_reads=int(1000) ):
    """ 
    It will read the R1 and R2 fastq files (or assume they are interleaved if fq2=None) and add R1 barcodes to R2 read names.
    """
    
    outfile = open(output, 'w')
    fq1 = read_file_by_lines(fq1file, top_million_reads * 1000 * 1000, 4)
    if fq2file != None:
        fq2 = read_file_by_lines(fq2file, top_million_reads * 1000 * 1000, 4)

    counter = 0
    start = time()
    for read1 in fq1:
        if fq2file != None:
            read2 = next(fq2)
        else:
            read2 = next(fq1)
        seq1 = read1[1].strip()
        counter += 1
        # Each 1M reads, report the progress;
        if counter % 1000000 == 0:
            #print("[info] Processed {}M lines, {}s per Million Reads".format(counter/1000000, round(time()-start, 2)))
            start = time()
            
        
        #get cell bc and UMI
        bc = extract_barcode(protocol, seq1)
        UMI = extract_UMI(protocol, bc, seq1, 0)
        
        #build new header and write fastq lines
        header = "_".join(['@', bc, UMI, str(counter)])
        seq = read2[1].strip()
        quality = read2[3].strip()
        outfile.writelines("\n".join([header, seq, "+", quality])+"\n")

    
    outfile.close()
    return 1

def count_barcodes(bam,output="bc.counts.txt"):
    print(ctime())
    print("Counting gene-tagged reads per barcodes in "+bam+"  ...")
    bam_file = pysam.Samfile(bam, 'rb')
    
    bcCt={}
    for read in bam_file.fetch():
        if(read.has_tag("gn") & (read.get_tag("HI")==1) & (read.mapq>=10)):
            bc=read.get_tag("XC")
            if bc in bcCt: bcCt[bc] +=1
            else: bcCt[bc]=1
    bam_file.close()

    bcCt_sorted=sorted(bcCt.items(),key=lambda x: x[1], reverse=True)
    with open(output,'w') as f:
        w = csv.writer(f)
        w.writerow(["barcode", "counts"])
        w.writerows(bcCt_sorted)
    print(ctime())
    print("...complete.")
    
def list_mm(bc):
    muts = mutate_single_base(bc)
    index = df_not_white.index.isin(muts)
    return df_not_white.loc[index].index.tolist()

def dedup_mm(bclist):
    return list(set(bclist)-mm_mult_matches)

def sum_mm(bclist):
    return sum(df_not_white.loc[bclist]["UMIs"])

def valid_barcode(protocol="", barcode_count="", min_UMIs=100, output="bc.stats.txt", doHamming=True, white_path=None, threads=1):
    """
    This function performs the following steps

        #. Reads in the barcode counts file (format: barcode,reads,UMIs)
        #. Filters to barcodes on the whitelist & nUMIs >= min_UMIs/10
        #. For each filtered whitelist barcode, mark non-whitelist barcodes with 1bp mismatch;
        #. Discard non-whitelist barcodes with multiple whitelist matches; aggregate marked non-whitelist barcodes with single whitelist match
        #. not done -->(Determine whether synthesis issue if last base shows A/T/C/G with similar ratio);       
        #. Filter aggregated barcodes by UMI counts (>=min_UMIs);
        #. Print the number of barcode and reads retained after each steps.

    Usage:
    ::
        from baseq.drops.barcode.stats import valid_barcode
        valid_barcode(protocol="", barcode_count="", min_UMIs=100, output="bc.stats.txt", doHamming=True, white_path=None, mm_min_UMIs=1)

    This writes a bc_stats.csv file (CSV) which contains:
    ::
        barcode/counts/mismatch_reads/mismatch_bc/mutate_last_base
    """
    
    print(ctime())
    df = pd.read_csv(barcode_count, index_col=0,sep=" ",names=['reads','UMIs']).sort_values("UMIs",ascending=False)
    df["whitelist"] = True

    #Determine which barcodes are on whitelist
    if white_path is not None:
        nBCs=len(df.index); 
        print("[info] Filtering "+str(nBCs)+" barcodes by whitelist...")
        bc_white = read_whitelist(protocol, white_path, None)
        df["whitelist"]=df.index.isin(list(bc_white[0]))
        df_white = df.loc[df['whitelist'] & (df['UMIs']>=min_UMIs/10)].copy().sort_values("UMIs", ascending=False)
        df_white['mismatch_UMIs'] = 0
        df_white['mismatch_BCs'] = 0
        df_white['mismatch_BC_names'] = ""

        

    #Aggregate by 1 Mismatch
    if not type(doHamming)==bool:
        doHamming=get_bool(doHamming,"doHamming")
    if doHamming:
        global df_not_white
        df_not_white = df.loc[np.logical_not(df['whitelist'])].copy()
        df_not_white["true_bc"]=""
        df_not_white["matched"]=False
        print("[info] "+str(len(df_white.index))+" barcodes with at least "+str(int(min_UMIs/10))+" UMIs on whitelist")
        print("[info] Aggregating remaining "+str(len(df_not_white.index))+" barcodes by hamming distance")
        pool=Pool(threads); white_mm=pool.map(list_mm, df_white.index.tolist()); pool.close()
        
        mm_matchCt=Counter(list(chain.from_iterable(white_mm)))
        global mm_mult_matches
        mm_mult_matches = set([k for k, v in mm_matchCt.items() if v > 1])
        pool=Pool(threads); white_mm_dd=pool.map(dedup_mm, white_mm); pool.close()
        pool=Pool(threads); white_mm_ct=pool.map(len, white_mm_dd); pool.close()
        pool=Pool(threads); white_mm_sum=pool.map(sum_mm, white_mm_dd); pool.close()
        pool=Pool(threads); white_mm_names=pool.map("_".join, white_mm_dd); pool.close()

        df_white['mismatch_UMIs'] = white_mm_sum
        df_white['mismatch_BCs'] = white_mm_ct
        df_white['mismatch_BC_names'] = white_mm_names

    df_white['total_UMIs'] = df_white['UMIs'] + df_white['mismatch_UMIs']

    print("[info] "+str(sum(df_white['mismatch_BCs']))+" barcodes and "+str(sum(df_white['mismatch_UMIs']))+" UMIs aggregated")

    #Filter by aggregated UMI counts
    print("[info] Keeping the barcodes with min UMIs: {}".format(min_UMIs))
    df_white_UMIs = df_white.loc[df_white['total_UMIs'] >= min_UMIs].sort_values("total_UMIs", ascending=False)

    #Print informations
    print("[info] Raw BC count {} ==> Whitelist BCs {} ==> Abundent BCs {}".format(len(df.index), len(df_white.index), len(df_white_UMIs.index)))
    print("[info] Raw UMIs {} ==> Whitelist UMIs {} ==> Abundent UMIs {}".format(sum(df['UMIs']), sum(df_white['total_UMIs']), sum(df_white_UMIs['total_UMIs'])))
    print("[info] Stats written to: {}".format(output))
    df_white_UMIs.to_csv(output)


def clean_bam(bam,cleanbam,bc_stats="bc.stats.txt"):
    df=pd.read_csv(bc_stats,index_col=0)
    bclist={}
    for bc in df.index:
        bclist[bc]=bc
        if df.loc[bc]['mismatch_BC_names'] == df.loc[bc]['mismatch_BC_names']:  # this is a way to test if its !nan
            for bcMM in df.loc[bc]['mismatch_BC_names'].split('_'):
                bclist[bcMM]=bc
    
    bam_in = pysam.Samfile(bam, 'rb')
    bam_out = pysam.Samfile(cleanbam, 'wb',template=bam_in)
    
    for read in bam_in.fetch():
        bc=read.get_tag("XC")
        if bc in bclist:
            read.set_tag("XC",bclist[bc])
            bam_out.write(read)
            
    bam_in.close()
    bam_out.close()
    



