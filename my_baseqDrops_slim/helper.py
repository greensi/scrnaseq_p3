import subprocess
import os, sys

def get_bool(myString,myVarName=""):
    if  myString.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif myString.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError(myVarName+' Boolean value expected')

def read_file_by_lines(filepath, maxLines, linecount):
    counter = 0
    if filepath.endswith("gz") or filepath.endswith("gzip") or filepath.endswith("gz2"):
        reading = subprocess.Popen(["gunzip", "-c", filepath], stdout=subprocess.PIPE, bufsize=1000000)
        infile = reading.stdout
        while True:
            counter = counter + 1
            if counter > maxLines:
                return
            data = [infile.readline().decode('utf8') for i in range(linecount)]
            if data[0] == "":
                return
            yield data
    else:
        infile = open(filepath, 'r')
        while True:
            counter = counter + 1
            if counter > maxLines:
                return
            data = [infile.readline() for i in range(linecount)]
            if data[0] == "":
                return
            yield data

def read_filelines(filepath, maxLines, linecount, skip=0):
    counter = 0
    maxLines = maxLines+skip
    if filepath.endswith("gz") or filepath.endswith("gzip") or filepath.endswith("gz2"):
        reading = subprocess.Popen(["gunzip", "-c", filepath], stdout=subprocess.PIPE, bufsize=1000000)
        infile = reading.stdout
        while True:
            counter = counter + 1
            if counter <= skip:
                continue
            if counter > maxLines:
                return
            data = [infile.readline().decode('utf8') for i in range(linecount)]
            if data[0] == "":
                return
            yield data
    else:
        infile = open(filepath, 'r')
        while True:
            counter = counter + 1
            if counter <= skip:
                continue
            if counter > maxLines:
                return
            data = [infile.readline() for i in range(linecount)]
            if data[0] == "":
                return
            yield data

def HammingDistance(seq1, seq2):
    return sum([1 for x in zip(seq1, seq2) if x[0] != x[1]])

def extract_barcode(protocol, seq):
    """Extract cell barcode from reads 1 sequences, if protocol not defined, return blank.
        - 10X: seq[0:16]
        - indrop: seq[0:i] + seq[i + 22 : i + 22 + 8] (i is length of barcode 1)
        - dropseq: seq[0:12]

    Usages:
    ::
        from baseqdrops import extract_barcode
        extract_barcode("10X", "ATCGATCGATCGACTAAATTTTTTT")

        #You can speficy the UMI and barcode length
        extract_barcode("10X", "ATCGATCGATCGACTAAATTTTTTT", 14, 5)

    Returns:
        barcode sequence, if no valid barcode, return ""

    """
    if protocol == "10X":
        return seq[0:16]
    elif protocol in ["10X_14_10", "10X_14_5"]: 
        return seq[0:14]
    elif protocol == "dropseq":
        return seq[0:12]
    elif protocol == "indrop":
        w1 = "GAGTGATTGCTTGTGACGCCTT"
        if w1 in seq:
            w1_pos = seq.find(w1)
            if 7 < w1_pos < 12:
                return seq[0:w1_pos] + seq[w1_pos + 22:w1_pos + 22 + 8]
        else:
            for i in range(8, 12):
                w1_mutate = seq[i:i + 22]
                if HammingDistance(w1_mutate, w1) < 2:
                    return seq[0:i] + seq[i + 22 : i + 22 + 8]
                    break
        return ""
    else:
        return ""   

def extract_UMI(protocol, barcode, seq1, missing_base=False):
    """
    Extract UMI from reads 1 sequences. If the reads length too short, return blank.
    ::
        10X: 16-26
        10X_14_10, 10X_14_5: 14:19/ 14:24
        indrop: seq1[len(barcode) + 22:len(barcode) + 22 + 6]
        dropseq: 11-19/12-20
    Args:
        barcode: Provide the barcode sequences;
        seq1: The sequence of read1;
    Usage:
    ::
        from baseqdrop import extract_UMI
        extract_UMI("", "", "AA....TTT", False)
    """
    UMI = ""
    seq1 = seq1.strip()
    if protocol == "10X" and len(seq1)>=26:
        UMI = seq1[16:26]
    if protocol == "10X_14_5" and len(seq1)>=19:
        UMI = seq1[14:19]
    if protocol == "10X_14_10" and len(seq1)>=24:
        UMI = seq1[14:24]
    if protocol == "dropseq" and len(seq1)>=20:
        if missing_base:
            UMI = seq1[11:19]
        else:
            UMI = seq1[12:20]
    if protocol == "indrop" and len(seq1)>=len(barcode) + 22 + 6:
        UMI = seq1[len(barcode) + 22:len(barcode) + 22 + 6]
    return UMI

def rev_comp(seq):
    ___tbl = {'A': 'T', 'T': 'A', 'C': 'G', 'G': 'C', 'N': 'N'}
    return ''.join(___tbl[s] for s in seq[::-1])

def mutate_single_base(seq):
    mutated = []
    bases = ['A', 'T', 'C', 'G']
    for index in range(0, len(seq)):
        temp = list(seq)
        base_raw = temp[index]
        for base in bases:
            if base != base_raw:
                temp[index] = base
                mutated.append(''.join(temp))
    return mutated

def read_whitelist(protocol, white_path, white_path2="NULL"):
    """
    Read Whitelist, get whitelist from config file: Drops/whitelistDir,
    Return: A set of whitelist barcodes
    """
    if not os.path.exists(white_path):
        sys.exit("WhiteList path does not exist: {}".format(white_path))
    print("[info] The white list is {}".format(white_path)) 
    if protocol == "indrop":
        bc_white = [set(), set()]
        with open(white_path, 'r') as infile:
            for line in infile:
                bc_rev = rev_comp(line.strip())
                bc_white[0].add(bc_rev)
        with open(white_path2, 'r') as infile:
            for line in infile:
                bc_rev = rev_comp(line.strip())
                bc_white[1].add(bc_rev)
    else:
        bc_white = [set()]
        with open(white_path, 'r') as infile:
            for line in infile:
                bc_white[0].add(line.strip())
    return bc_white

def check_whitelist(bc_white, protocol, barcode):
    """
    Check whitelist.

    Usage:
    ::
        check_whitelist(bc_white, "10X", "ATTATATATT")
    """
    if protocol == "indrop":
        if barcode[0:-8] in bc_white[0] and barcode[-8:] in bc_white[1]:
            return 1
        else:
            return 0
    else:
        if barcode in bc_white[0]:
            return 1
        else:
            return 0

def list_mm(bc):
    muts = mutate_single_base(bc)
    index = df_not_white.index.isin(muts)
    return df_not_white.loc[index].index.tolist()

def dedup_mm(bclist):
    return list(set(bclist)-mm_mult_matches)

def sum_mm(bclist):
    return sum(df_not_white.loc[bclist]["UMIs"])
