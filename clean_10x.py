import os, sys, getopt
from multiprocessing import  Pool
from collections import Counter
from itertools import product, chain
from my_baseqDrops_slim.main import count_barcodes, valid_barcode,clean_bam
from my_baseqDrops_slim.helper import get_bool,mutate_single_base

### get command line arguments
fullCmdArguments = sys.argv
argumentList = fullCmdArguments[1:]
unixOptions = "i:o:p:c:m:s:hw:t:"  
gnuOptions = ["inbam=","outbam=","protocol=","counts=","min_UMIs=","stats=","hamming","whitelist=","threads="]  
try:  
    arguments, values = getopt.getopt(argumentList, unixOptions, gnuOptions)
except getopt.error as err:  
    # output error, and return with an error code
    print (str(err))
    sys.exit(2)

dohamming=False
whitelist=None
min_UMIs=100
mm_min_UMIs=1
for arg, val in arguments:  
    if arg in ("-i", "--inbam"):
        inbam=val
    elif arg in ("-o", "--outbam"):
        outbam=val
    elif arg in ("-p", "--protocol"):
        protocol=val
    elif arg in ("-c", "--counts"):
        bc_counts=val
    elif arg in ("-m", "--min_UMIs"):
        min_UMIs=int(val)
    elif arg in ("-s", "--stats"):
        bc_stats=val
    elif arg in ("-h", "--hamming"):
    	dohamming=True
    elif arg in ("-w", "--whitelist"):
        whitelist=val
    elif arg in ("-t", "--threads"):
        threads=int(val)
         


    
print("Examining barcodes in: " , bc_counts)
if dohamming:
	print("--Aggregating barcodes within 1 hamming distance using "+str(threads)+" threads")
if whitelist is not None:
	print("--Keeping barcodes on the whitelist ", whitelist)
print("--Filtering out barcodes with less than ", min_UMIs , " UMIs")
print("Writing results to: " , bc_stats)

## run steps
valid_barcode(protocol, bc_counts, min_UMIs, bc_stats, dohamming, whitelist, threads)
clean_bam(inbam,outbam,bc_stats)
    