#!/bin/bash

## reads in jira table, extracts pool, protocol and species info for each library from jamo
## creates a new config file for each unique pool/protocol/species combo
## finds ref genome files in phytozome, creates script to prepare ref data
## writes sample info line to configs/scRNA/${POOL_ID}_protocol_species/libs

## usage
usage="usage: $0 <JIRA_TABLE> <CONFIG_DIR (default $HOME/configs/scRNA/)> <TRIM_R1_ds (default 25)> <TRIM_R1_10x (default 0)> <MIN_UMIS_PER_CELL (default 1000)>"
if [ -z $1 ]; then echo $usage; exit 1; fi

## make sure shell can handle arrays; initialize array for new configs
whotest[0]='test' || (echo 'Failure: arrays not supported in this version of bash.' && exit 2)
## make sure gtfToGenePred is on path
which gtfToGenePred > /dev/null 2>&1 || (echo 'gtfToGenePred must be on path.' && exit 2)

declare -a config_names	

## input parameters
JIRA_TABLE=$1
CONFIG_DIR=${2:-"$HOME/configs/scRNA/"}
TRIM_R1_ds=${3:-25} ## only used for dropseq
TRIM_R1_10x=${4:-0} ## only used for 10x
MIN_UMIS_PER_CELL=${5:-1000}

## parse jira table column headers
libField=$(head -1 $JIRA_TABLE | sed 's/ //g' | awk -F ',' '{ for(ii=1;ii<=NF;ii++){if($ii ~ /library/){libField=ii}}; print(libField)}')
fileField=$(head -1 $JIRA_TABLE | sed 's/ //g' | awk -F ',' '{ for(ii=1;ii<=NF;ii++){if($ii ~ /seq_unit_name/){sunField=ii}}; print(sunField)}')

## list info to grab from jamo
infoFields="metadata.library_name,\
file_name,\
metadata.sow_segment.sample_name,\
metadata.parent_library_name,\
metadata.sow_segment.genus,\
metadata.sow_segment.species,\
metadata.sow_segment.strain,\
metadata.sow_segment.ncbi_tax_id,\
metadata.physical_run.dt_sequencing_end,\
metadata.physical_run.instrument_type,\
metadata.read_stats.file_num_reads,\
metadata.rqc.read_qc.bwa_aligned_duplicate_percent,\
metadata.rqc.read_qc.contam_jgi_contaminants_percent,\
metadata.rqc.read_qc.contam_rrna_percent,\
metadata.rqc.read_qc.contam_mitochondrion_percent,\
metadata.sdm_seq_unit.index_name,\
metadata.sequencing_project.sequencing_project_name,\
metadata.sow_segment.sample_isolated_from,\
metadata.sequencing_project.comments"


## read jira table line by line
tail -n +2 $JIRA_TABLE | sed 's/ //g' | while read line; do

	## get library info
	libID=$(echo $line | cut -f$libField -d','); echo $libField $libID
    jamoFile=$(echo $line | cut -f$fileField -d','); echo $fileField $jamoFile
    libInfo=$(jamo report select $infoFields where file_name=$jamoFile as csv | tail -n +2)

	## assign library to config group
	config_name=$(echo $libInfo | awk -F ',' '{
			name=$17;pool=$4;genus=$5;species=$6;
			if(toupper(name) ~ /10X/){protocol="10X"}else{protocol="dropseq"}
			spcPrefix=substr(genus,1,1)""substr(species,1,1)
			print(pool"_"protocol"_"spcPrefix)
	}'
	)
	
	## if config group doesnt exist yet
	if [ ! -e $CONFIG_DIR/$config_name ]; then
		## make new dir
		echo "creating new config for $config_name"
		mkdir $CONFIG_DIR/$config_name
		config_names+=("$config_name")
		## initialize libs list
		echo $infoFields | sed 's/metadata.//g' > $CONFIG_DIR/$config_name/libs.list
		
		## get ref genome info
		refPrefix=$(echo $config_name | cut -f3 -d'_')
		refName=$(echo $libInfo | awk -F ',' '{genus=$5;species=$6;print(substr(genus,1,1)""species)}')
		taxid=$(echo $libInfo | cut -f8 -d',')

		## if ref genome prep files dont exist yet
		if [ ! -e $SCRATCH/refData/$refName ]; then 
			$(dirname $0)/prepare_ref_data.sh $refName $refPrefix $taxid
		fi

		protocol=$(echo $config_name | cut -f2 -d'_')
		if [ $protocol == "dropseq" ]; then TRIM_R1=$TRIM_R1_ds; else TRIM_R1=$TRIM_R1_10x; fi

		## print parameter definitions to config
		echo "
		PROTOCOL=$protocol
		NAME=$config_name
		POOL_ID=$(echo $config_name | cut -f1 -d'_')
		FASTQ_DIR=$HOME/seqData/scRNA/$config_name
		LIBS_LIST=$CONFIG_DIR/$config_name/libs.list
		REF_DIR=$SCRATCH/refData/$refName
		OUT_DIR=$SCRATCH/analysis/scRNA/$config_name
		TRIM_R1=$TRIM_R1
		HAMMING=TRUE
		WHITELIST=$HOME/scripts/baseqDrops/repo/baseqDrops/whitelist/whitelist.10X.txt 
		MIN_UMIS_PER_CELL=$MIN_UMIS_PER_CELL
		" | sed 's/\t//g' > $CONFIG_DIR/$config_name/config
	fi

	echo "writing info for $libID to $CONFIG_DIR/$config_name/libs.list"
	echo $libInfo >> $CONFIG_DIR/$config_name/libs.list
	$(dirname $0)/link_fastqs.sh $libID $jamoFile $HOME/seqData/scRNA/$config_name $TRIM_R1

done

echo "the following config files were created:
${config_names[@]}
"
