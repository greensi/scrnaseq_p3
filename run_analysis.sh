##!/bin/bash

## Wrapper script to run raw fastq files from a dropseq or 10X experiment through a custom pipeline drawing heavily on:
## 1. the steps described in:
##     Zhang et al 2019, Molecular Cell  doi.org/10.1016/j.molcel.2018.10.020 
## 	and implemented in the python package baseqDrops found at:
##     https://github.com/beiseq/baseqDrops
## 2. the steps described in:
##		Macosko et al 2015, Cell doi.org/10.1016/j.cell.2015.05.002
##	and implemented in the java scripts at:
##      https://github.com/broadinstitute/Drop-seq
## Author: Sharon Greenblum
## Institute: Joint Genome Institute
## Date: July 9, 2019



allSteps=trim,align,clean,dge,archive

usage()
{
	echo "usage: run_analysis.sh configFile [steps=all] [time=6:00:00] [cpus=32] [qos=genepool]

	** steps must be one or more (comma-separated) of the following keywords:
	
			$allSteps
	
			or 

			all[default]
	
	"
}
if [ -z $1 ]; then usage; exit 1;  fi

## set default options
mysteps=all
time=6:00:00
cpus=32
qos=genepool
shortAlign=0

## set paths and tools
currentDir=$PWD
scriptsDir=$(dirname $0)
dstDir=$scriptsDir/drop-seq_v2.3
bbtools="shifter --image=docker:bryce911/bbtools:latest"
STAR="shifter --image=docker:mgibio/star STAR"
samtools="shifter --image=docker:mjblow/samtools-1.9:latest samtools"
source activate scRNA_pipeline
mypython=$(which python)
adapterseq=AAGCAGTGGTATCAACGCAGAGTGAATGGG
adapterlen=$(echo $adapterseq | wc -c | awk '{print ($1 - 1)}')
starVersion=$(${STAR} --version | grep -o [0-9].[0-9] )

## get variables from command line
config=$1
for i in $(echo "$@" | awk '{$1="";print $0}'); do
case $i in
	steps=*)
    	mysteps="${i#*=}"
    	shift 
    ;;
    time=*)
    	time="${i#*=}"
    	shift 
    ;;
    cpus=*)
    	cpus="${i#*=}"
    	shift 
    ;;
    qos=*)
    	qos="${i#*=}"
    	shift 
    ;;
    --shortAlign)
    	shortAlign=1
    	shift 
    ;;    
    *)  # unknown option
        echo "unknown command line argument: $i"; usage; exit 1;  
    ;;
esac
done

## parse config file
$scriptsDir/parse_config.sh $config

## read in params, copy to output directory
. $(dirname $config)/params.txt
cp $(dirname $config)/params.txt $OUT_DIR

echo "############################"
echo "# running scRNA analysis pipeline for "
echo "# [ $(echo $libIDs | tr ',' ' ' | wc -w) ] [ $PROTOCOL ] libraries:"
echo "# [ $libIDs ]"
echo "# aligning to genomes in [ $REF_DIR ]"
echo "# and writing analysis output files to [ $OUT_DIR ]
############################"

## set up location of wrapper files
if [ ! -e $OUT_DIR/scripts ]; then mkdir -p $OUT_DIR/scripts; fi
libPrepWrapper=$OUT_DIR/submit_sbatch_jobs.sh
echo "#!/bin/bash" > $libPrepWrapper
dependString=""

## parse steps
if [ $mysteps == "help" ]; then usage; exit 1; fi
if [ $mysteps == all ]; then mysteps=$allSteps; fi
stepString=""
for step in $(echo $allSteps | tr ',' '\n' ); do
	stepString=$stepString$( echo $mysteps | grep -c $step)" "
done


###############
## run libraries
################

for libID in $( echo $libIDs | tr ',' '\n'); do
	
	## define command file and write sbatch command to libPrepWrapper
	libCmdsFile=$OUT_DIR/scripts/run_${libID}
	echo "
	sbatch $dependString -J scRNA.$libID -q $qos -C haswell -A gtrnd -t $time -N 1 -n 1 -c $cpus \
	-o $libCmdsFile.out -e $libCmdsFile.err $libCmdsFile.sh $stepString
	" >> $libPrepWrapper 
	

	## if commands file already exists, you're done.. move to next lib
	if [ -e $libCmdsFile.sh ]; then echo "$libCmdsFile.sh already exists - delete and re-run to edit"; continue; fi

	## otherwise.....
		
	#set up out directory 
	outdir=$OUT_DIR/$libID
	if [ -e $outdir ]; then 
		echo "script produced will overwrite files in $outdir - be sure this is what you want before running"; 
	else
		mkdir -p $outdir
	fi
		
	#fastq=$FASTQ_DIR/$libID.fastq.gz
	trimmedfastq=$libID.trimmed.fastq.gz
	bam=$libID.bam
	tmpbam=$libID.tmp.bam
	finalbam=$libID.cleaned.bam
	dge=$libID.dge
	log=$libID.log

	### commands
	if [ $INTERLEAVED == "TRUE" ]; then
		readFastq="zcat $FASTQ_DIR/$libID.fastq.gz"
	else	
		R1=$(ls $FASTQ_DIR/$libID*R1*.fastq*); 
		R2=$(ls $FASTQ_DIR/$libID*R2*.fastq*);
		readFastq="$bbtools reformat.sh in=$R1 in2=$R2 out=stdout.fq"
	fi
	trimR1adapter="$bbtools bbduk.sh in=stdin.fq int=t skipr2=t literal=ACGTN k=1 ktrim=l restrictleft=$TRIM_R1 out=stdout.fq unbgzip=f 2>> trim.report.txt "
	trimR2adapter="$bbtools bbduk.sh in=stdin.fq int=t skipr1=t literal=$adapterseq ktrim=l k=$adapterlen hdist=2 mink=5 hdist2=0 rcomp=f minlength=10 out=stdout.fq unbgzip=f 2>> trim.report.txt"
	trimR2polyA="$bbtools bbduk.sh in=stdin.fq int=t skipr1=t ktrim=r k=6 hdist=0 literal=AAAAAA rcomp=f minlength=10 out=stdout.fq unbgzip=f 2>> trim.report.txt"
	addBCtoReadName="$mypython $scriptsDir/tag_fastq.py -p $PROTOCOL -o /dev/stdout -f /dev/stdin"
	aligncpus=$(echo $cpus | awk '{if($1-6 >1){print $1-6} else{print $1-1}}')
	sortcpus=$(($cpus-$aligncpus))

	trim=$(echo " echo '#trimming stats' > trim.report.txt; $readFastq | " $([ $TRIM_R1 -gt 0 ] && ( echo "$trimR1adapter |") || ( echo "")) "$trimR2adapter | $trimR2polyA | $addBCtoReadName | gzip -c > $trimmedfastq" )
	align="${STAR} \
	--genomeDir $REF_DIR/STAR$starVersion \
	--runThreadN $aligncpus \
	--outFileNamePrefix align. \
	--outBAMsortingBinsN 65 \
	--limitBAMsortRAM 11000000000 \
	--outStd BAM_SortedByCoordinate --outSAMtype BAM SortedByCoordinate \
	--readFilesIn $trimmedfastq \
	--readFilesCommand zcat"
	if [ $shortAlign -gt 0 ]; then 
		align=$align" \
		--outFilterScoreMinOverLread 0 \
		--outFilterMatchNminOverLread 0 \
		--outFilterMatchNmin 0 \
		--outFilterMismatchNmax 2
		"
	fi
	align=$(echo $align | tr '\t' ' ')
	 #--outBAMsortingBinsN 100 : this tells star to divide the genome into 100 bins instead of the default 50 for sorting... means it writes more files to disk but uses less memory
	 #--limitBAMsortRAM 11000000000 : this tells star to use 11G of ram to sort the reads after mapping.  the default behavior is to use 6 threads and RAM = the total size of the genome+index (~4G for Bd ~3G for At); instead we tell it to use (almost) all of the RAM in the 6 cpus
	tagCellUMI="$samtools view -h -F 260 -q 10 | awk -F '_' '(\$1 ~ /^@/){print}(\$1 !~ /^@/){print \$0\"\\tXC:Z:\"\$2\"\\tXM:Z:\"\$3}' " #| $samtools sort -m 1G -@ $sortcpus -o - - 
	tagGene="$dstDir/TagReadWithGeneFunction O=/dev/stdout ANNOTATIONS_FILE=$REF_DIR/$refPrefix.gtf INPUT=/dev/stdin SUMMARY=tag.geneReport.txt"
	geneFilterBam="$samtools view -h - | awk -v outfile=\"tag.geneReport.txt\" 'BEGIN{print \"GENIC\tTOTAL\" >> outfile; genicCt=0; totalCt=0};(/^@/){print};(! /^@/){totalCt++; if(! /XF:Z:INTERGENIC/){print;genicCt++}}; END{print genicCt\"\t\"totalCt >> outfile}' | $samtools view -hb -"
	## keep only genic reads (CODING / UTR / INTRONIC regions) from SENSE strand
	indexBam="$samtools index $bam"
	getBamStats="$dstDir/BamTagHistogram INPUT=$bam TAG=XC O=tag.cellRdCts.gz; $dstDir/BamTagOfTagCounts -m 8G INPUT=$bam PRIMARY_TAG=XC SECONDARY_TAG=XM O=tag.cellUMICts.gz; join -j1 2 -j2 1 <(zcat tag.cellRdCts.gz | sort -k2,2) <(zcat tag.cellUMICts.gz | cut -f1-2 | sort -k1,1) | sort -k3,3gr | gzip -c > tag.cellRdUMICts.gz"
	
	cleanBam=$(case $PROTOCOL in
		"dropseq" ) echo "$dstDir/DetectBeadSubstitutionErrors INPUT=$bam OUTPUT=$tmpbam NUM_THREADS=$aligncpus TMP_DIR=$outdir MIN_UMIS_PER_CELL=20 OUTPUT_REPORT=clean.subReport.txt; $dstDir/DetectBeadSynthesisErrors INPUT=$tmpbam NUM_THREADS=$aligncpus MIN_UMIS_PER_CELL=20 OUTPUT_STATS=clean.synthStats.txt SUMMARY=clean.synthSummary.txt REPORT=clean.synthReport.txt CREATE_INDEX=true TMP_DIR=$outdir OUTPUT=$finalbam"
		;;
		"10X" ) echo "$mypython $scriptsDir/clean_10x.py -i $bam -o $finalbam -p 10X -c tag.cellRdUMICts.gz -m $MIN_UMIS_PER_CELL -s clean.bcStats.txt --hamming -w $WHITELIST -t $aligncpus > clean.10XReport.txt"
		;;
	esac)
	
	makeDGE="$dstDir/DigitalExpression I=$finalbam O=$dge.gz SUMMARY=dge.summary.txt MIN_NUM_TRANSCRIPTS_PER_CELL=$MIN_UMIS_PER_CELL; $scriptsDir/get_stats.sh $config $libID"
	archive="$scriptsDir/make_json_for_jamo.sh $config $libID"
	logTime="\`date \"+%Y%m%d-%H%M%S\"\`,\$SECONDS >> $log; SECONDS=0"

	#write to commands file
	echo "#!/bin/bash" > $libCmdsFile.sh
	echo -e 'doTrim=$1; doAlign=$2; doClean=$3; doDGE=$4; doArchive=$5' >> $libCmdsFile.sh
	echo "module load python; source activate scRNA_pipeline" >> $libCmdsFile.sh
	echo "cd $outdir"  >> $libCmdsFile.sh
	echo "SECONDS=0" >> $libCmdsFile.sh
		
	echo "
	
	if [ \$doTrim -eq 1 ] ; then 
		$trim ; 
		echo trim,$logTime 
	fi

	if [ \$doAlign -eq 1 ] ; then 
		$align | $tagCellUMI | $tagGene | $geneFilterBam > $bam ; $indexBam;  $getBamStats ; 
		echo align,$logTime
	fi

	if [ \$doClean -eq 1 ] ; then 
		$cleanBam ; 
		echo clean,$logTime
	fi

	if [ \$doDGE -eq 1 ] ; then 
		$makeDGE; 
		echo dge,$logTime
	fi

	if [ \$doArchive -eq 1 ] ; then 
		$archive; 
		echo archive,$logTime
	fi
	
	" >> $libCmdsFile.sh
	
	chmod +x $libCmdsFile.sh

done

chmod +x $libPrepWrapper
echo "
# prep libraries by running
$libPrepWrapper
"
