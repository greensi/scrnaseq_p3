#!/bin/bash
lib=$1

field=${2:-analysis_project_id}

if [ $field = "sequencing_project_id" ]; then 
	echo $(curl https://rqc.jgi-psf.org/api/file_search/search/${lib} 2> /dev/null | grep seq_proj_id |  sed 's/,$//' | cut -f2 -d':')
elif [ $field = "analysis_project_id" ]; then 
	echo $(curl https://projects.jgi.doe.gov/pmo_webservices/analysis_tasks_for_library/$lib 2> /dev/null | tr ',' '\n' | tr '{' '\n' | grep "^\"analysis_project_id" | cut -f2 -d':')
else 
        echo $(curl https://rqc.jgi-psf.org/api/file_search/search/${lib} 2> /dev/null | grep $field )
fi

#curl https://projects.jgi.doe.gov/pmo_webservices/analysis_tasks_for_library/${lib} 2> /dev/null | \
#tr ',' '\n' | \
#sed 's/\"//g' | \
#tr '{' '\n' | \
#tr '}' '\n' | \
#grep "^$field"


### to do lookup with sequencing projectID instead:
#curl https://proposals.jgi.doe.gov/pmo_webservices/analysis_projects?sequencing_project_id=${spID} 2>/dev/null | \



