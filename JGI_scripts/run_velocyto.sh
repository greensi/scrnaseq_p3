#!/bin/bash
bam=$1
gtf=$2
batchmode=${3:-"no"}

# eg: 
#bam=/global/cscratch1/sd/greensi/analysis/scRNA/GOBXN_10x_At/Arabidopsis_2_SI-GA-B12/Arabidopsis_2_SI-GA-B12.cleaned.bam
#gtf=$SCRATCH/refData/AUTO-272571/At.with_exon_num.gtf
### note that the gtf must have exon_number as a field in the 9th column
### try running: scrnaseq_p3/JGI_scripts/add_exon_number_to_gtf.R to grab this info from a gff3 file

## use flag -b to run in batch mode
usage()
{
	echo "
	usage: run_velocyto.sh bamFile gtfFile [-b]
	** use flag -b to run in batch mode
	"
}
if [ -z $1 ] || [ $1 == "help" ]; then usage; exit 1;  fi


bamDir=$(dirname $bam)
sampName=$(basename $bam | sed 's/.cleaned//')
log=$(dirname $(dirname $bam))/scripts/velocyto_${sampName}

if [ $batchmode == "-b" ]; then 

	echo "sbatch  -J velocyto.$sampName -q genepool -A gtrnd -t 2:00:00 -N 1 -n 1 -c 32 -o $log.out -e $log.err $0 $bam $gtf"

else

	outDir=$bamDir/velocyto
	barcodes=$bamDir/${sampName}.final_barcodes.list

	tail -n +8 $bamDir/dge.summary.txt | cut -f1 > $barcodes
	rm $outDir/${sampName}_*.loom; 

	shifter --image=jsschrepping/velocyto-docker:jss_v0.0.2 velocyto run \
	-b $barcodes -o $outDir $bam $gtf
fi
