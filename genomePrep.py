#!/usr/bin/env python

desc='''
    Script to create prep.sh to prepare genome(s) with cellranger

    Author: Vasanth Singan & Sharon Greenblum
    Institute: Joint Genome Institute
    Date: June 11, 2019
'''

log='''
    June 18, 2019 - First commit
'''

import sys
import os
import argparse

def getArgs():
    '''Parse arguments'''
    parser = argparse.ArgumentParser(description=desc+CONFIG_EXAMPLE,formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('config_file', help="config.txt")
    parser.add_argument('-s','--star', help="path to STAR executable; ie: shifter --image=vrsingan/star:v2.5.4a STAR")
    parser.add_argument('-d','--dropseqtools', help="path to dropseq-tools directory; ie: /global/homes/g/greensi/scripts/scRNAseq_P3/drop-seq_v2.3")
    args = parser.parse_args()
    return args

CONFIG_EXAMPLE ="\n\nExample config.txt\n==================\n"
CONFIG_EXAMPLE += "#ID\t#Genome_full_path\tGff3_full_path\n"
CONFIG_EXAMPLE += "BD\t/path/to/Bdistachyon_314_v3.0.fa\t/path/to/Bdistachyon_314_v3.1.gene.gff3\n"

def parseConfig(config):
    ds = {}
    with open(config,"r") as f:
        lines = f.readlines()
        for line in lines:
            if not line.startswith("#"):
                tokens = line.strip().split('\t')
                if tokens[0] not in ds:
                    ds[tokens[0]] = []
                ds[tokens[0]].append(tokens[1])
                ds[tokens[0]].append(tokens[2])
    return ds

def prepGenome(config, mystar,mydropseqpath):
    mybbtools="shifter --image=bryce911/bbtools"
    mycufflinks="shifter --image=dmccloskey/cufflinks"
    #mystar="shifter --image=vrsingan/star:v2.5.4a STAR"
    mysamtools="shifter --image=mjblow/samtools-1.9:latest samtools"
    mybgzip="shifter --image=stackleader/bgzip-utility:latest bgzip"
    #mydropseqpath=sys.path[0]
    scriptDir=os.path.dirname(os.path.realpath(sys.argv[0]))

    ds = parseConfig(config)
    cmd = ""
    prefix = ""
    for gid in ds: ## for each species
        if prefix == "":
            prefix = gid ## add species code to prefix
        else:
            prefix = prefix+"_"+gid
        cmd += "\n\n"+mybbtools+" reformat.sh -Xmx2G ow=t trd in="+ds[gid][0]+" out="+gid+"_tmp.fasta" ## reformat to remove anything after first space from chrom/contig names
        cmd += "\ncat "+gid+"_tmp.fasta | sed 's/>/>"+gid+"_/g' >"+gid+".fasta" ## add species code to beginning of contig names
        
        gffParts=os.path.splitext(ds[gid][1])
        if (gffParts[1] == ".tar"):
            filename = "untar_"+os.path.split(gffParts[0])[1]
            cmd += "\ntar -xOf "+ds[gid][1]+" > "+filename
            ds[gid][1]=filename
        os.system("file "+ds[gid][1]+" | grep -c gzip > tmp")
        if(int(open('tmp', 'r').read()) > 0):
            gffcat="zcat"
        else:
            gffcat="cat"
        cmd += "\n"+gffcat+" "+ds[gid][1]+" | "+mycufflinks+" gffread - -T -o- | awk '{FS=\"\\t\";OFS=\"\\t\"}$1!~/#/{print \""+gid+"_\"$0}' >"+gid+".gtf" ## convert gff3 to gtf
    print(prefix)
    cmd += "\n\n"
    if len(ds) > 1:  ## if more than one species, combine into megagenome
        cmd += "\nrm "+prefix+".fasta"
        cmd += "\nrm "+prefix+".gtf"
        for gid in ds:
            cmd += "\ncat "+gid+".fasta >>"+prefix+".fasta" ## cat together the fastas
            cmd += "\nrm "+gid+".fasta"
            cmd += "\ncat "+gid+".gtf >>"+prefix+".gtf" ## cat together the gtfs
            cmd += "\nrm "+gid+".gtf"
    cmd += "\nsort -k1,1 -k3,3n "+prefix+".gtf > tmp.gtf" ## sort the gtf by chrom/contig then by start coordinate
    cmd += "\nmv tmp.gtf "+prefix+".gtf"

    ### make a table associating gene names (as used by dropseqtools for DGE table headers) with chromosomes
    cmd += "\n"+scriptDir+"/make_gene_map_from_gtf.sh "+prefix+".gtf"

    ## do the rest with provided drop-seq script
    cmd += "\n mv "+prefix+".fasta "+prefix+".tmp.fasta"
    ## have to add extra dummy columns to gtf for it to work 
    cmd += "\n cat "+prefix+".gtf | awk '{addMe=\"\"; for(ii=1;ii<=NF;ii++){if($ii==\"transcript_id\"){tID=$(ii+1)};if($ii==\"gene_id\"){gID=$(ii+1)}}} (!/transcript_name/){addMe=addMe\" transcript_name \"tID} (!/gene_name/){addMe=addMe\" gene_name \"gID} {print $0,addMe}' | sed 's/geneID/gene_id/' > "+prefix+".tmp.gtf" 
    cmd += "\n"+mydropseqpath+"/create_Drop-seq_reference_metadata.sh -n "+prefix+" -r "+prefix+".tmp.fasta -g "+prefix+".tmp.gtf -s "+prefix+" -d "+mydropseqpath+" -a \'"+mystar+"\' -b \'"+mybgzip+"\' -i \'"+mysamtools+"\'"
    cmd += "\n"+"gtfToGenePred "+prefix+".gtf refFlat.tmp.txt -genePredExt -geneNameAsName2"
    cmd += "\n"+"paste <(cut -f 12 refFlat.tmp.txt) <(cut -f 1-10 refFlat.tmp.txt) > "+prefix+".refFlat"
    cmd += "\n rm *tmp* untar_*"
    cmd += "\n echo genome prep complete!"
    
    ## write all commands to file
    with open('prep.sh','w') as f:
        f.write('#!/bin/bash -l\n')
        f.write('#$ -S /bin/bash\n')
        f.write(cmd)
    print(cmd)

if __name__ == "__main__":
    try:
        args=getArgs()
        if not os.path.isfile(args.config_file):
            sys.stderr.write("ERROR: input file '%s' does not exist. Exiting.\n" % args.config_file)
            sys.exit(1)
        else:
            prepGenome(args.config_file, args.star,args.dropseqtools)
    except KeyboardInterrupt:
        pass

