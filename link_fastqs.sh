#!/bin/bash
usage="usage: $0 <libID> <jamoFile> <FASTQ_DIR> <TRIM_R1(default 0)> "

libID=$1
jamoFile=$2
FASTQ_DIR=$3
TRIM_R1=${4}

if [ -z $1 ]; then echo $usage; exit 1; fi
if [ ! -e $FASTQ_DIR ]; then echo "creating fastq directory: $FASTQ_DIR"; mkdir -p $FASTQ_DIR; fi

status=$(jamo fetch filename $jamoFile | cut -f2 -d' ')
if [ $status == PURGED ] || [ $status == RESTORE_IN_PROGRESS ]; then
	echo "$libID status: $status --try again later"
else
	fastq=${FASTQ_DIR}/${libID}.fastq.gz
	jamo link filename $jamoFile 1>&2 ; mv $jamoFile $fastq;
	echo "link created: "$(ls -lLh $fastq | awk '{{printf $5"\t"$NF"\n"}}') 
	pctR1Adapter=$(zcat $fastq | grep -A1 "1:N" | grep -v "^@" | head -200 | grep -c "^AAGCAGTGGTATCAACGCAGAGTAC")
	if [ $pctR1Adapter -gt 5 ] && [ $TRIM_R1 -eq 0 ]; then 
		echo "adapter detected in ${pctR1Adapter}% of R1 reads, but TRIM_R1 is not set --edit config and retry"
	fi
	if [ $pctR1Adapter -lt 5 ] && [ $TRIM_R1 -gt 0 ]; then 
		echo "adapter detected in ${pctR1Adapter}% of R1 reads, but TRIM_R1 is set to $TRIM_R1 --edit config and retry"
	fi

fi


	
