#!/bin/sh

######### 
### TO BE ADDED TO THE PREPARE_REF SCRIPT
#######
### the default behavior for dropseq tools is to annotate reads with gene *names*, not gene *IDs*
### but in many gtf files, gene names are not unique... ie multiple gene IDs may have the same name.  this may indicate misannotation, or it may just be non-specific names for genes that are truly distinct
### dropseq tools skips these genes when it encounters them.  that may be removing real information
### to get around this, the following script reformats the gtf file so the geneID is prepended to every gene name
### the original gtf file is preserved as ${gtf}.orig

gtf=$1
id_field=${2:-1}  ### which field of the gene_id string contains the actual ID? 

cat $gtf | awk -v id_field="$id_field" -F '\t' '
BEGIN{ OFS = "\t"}
{
	split($9,ann,";");  ### tab-demarcated field 9 contains the relevant info
	for(ii=1;ii<=length(ann);ii++){  ### for each item in field 9
		split(ann[ii],annf,"\"");  ### split it by quotion marks
		if(annf[1]==" gene_id "){split(annf[2],gid,".");gene_id=gid[id_field]};  ### if the label is gene_id, split the entry by '.' and extract the indicated field 
		if(annf[1]==" gene_name "){gene_name=annf[2]}  ### if the label is gene_name, take the whole thing
	}; 
	new_name="gene_name \""gene_id"."gene_name"\""; #### create a new gene_name by pasting together id and name
	old_name="gene_name \""gene_name"\"";
	sub(old_name,new_name,$9); ### substitute it in
	print   
}' > ${gtf}.temp

mv $gtf ${gtf}.orig
mv ${gtf}.temp $gtf
