#!/bin/bash

config=$1
libID=$2 
jiraKey=${3:-none}
scriptDir=$(dirname $0)

. $(dirname $config)/params.txt

##########
## copy files for submission to new directory 
#############
jatDir=$OUT_DIR/$libID/submit_to_jat
if [ ! -e $jatDir ]; then mkdir $jatDir; fi

cp $(dirname $config)/params.txt $jatDir
cp $OUT_DIR/scripts/run_${libID}.sh $jatDir
tar -czvf $jatDir/$libID.logs.tar.gz $OUT_DIR/$libID/*.txt $OUT_DIR/$libID/*.out
cp $OUT_DIR/$libID/$libID.stats $jatDir
cp $OUT_DIR/$libID/$libID.cleaned.bam $jatDir
cp $OUT_DIR/$libID/$libID.dge.gz $jatDir

###########
## set read + execute permissions
############
chmod o=rx $jatDir; chmod o=rx $OUT_DIR/$libID; chmod o=rx $OUT_DIR
chmod o=rx $jatDir/*

#############
#### get library metadata
#############
spID=$($scriptDir/JGI_scripts/get_ITS_info_for_library.sh $libID sequencing_project_id )
apID=$($scriptDir/JGI_scripts/get_ITS_info_for_library.sh $libID analysis_project_id )
fastqFile=$(jamo info library $libID | cut -f2 -d' ')
gffJAT=$(basename $REF_DIR | tr '_' '\n' | head -n -1)
genomeJAT=$(echo '['$(for jat in $gffJAT; do echo \"$(jamo report select metadata.parent_refs where metadata.jat_key = $jat | cut -f2 -d"'")\"; done)']' | tr ' ' ',')
spcName=$(echo '['$(for jat in $gffJAT; do echo \"$(jamo report select metadata.reference_name where metadata.jat_key = $jat)\"; done)']' | sed  's/\" \"/\",\"/g ')
spcID=$(echo '['$(for jat in $gffJAT; do echo $(jamo report select metadata.ncbi_taxon_id where metadata.jat_key = $jat); done)']' | tr ' ' ',')
gffJAT=$(echo '['$(for jat in $gffJAT; do echo \"$jat\";done)']' | tr ' ' ',')
starVersion=$(cat $OUT_DIR/scripts/run_${libID}.sh | grep -o STAR[0-9][0-9.]*)

#############
### write json file
#############
echo '
{
    "metadata":{
        "pipeline_version":"0.0",
        "library_name":"'$libID'",
        "sequencing_project_id":'$spID',' > $jatDir/metadata.json
if [ ! -z $apID ]; then
	echo '	"analysis_project_id":'$apID',' >> $jatDir/metadata.json
fi
echo '	"jira_key":"'$jiraKey'",
        "input_reads_fastq":"'$fastqFile'",
        "reference_genome_jat_key":'$genomeJAT',
        "reference_annotation_jat_key":'$gffJAT',
        "ncbi_org_name":'$spcName',
        "ncbi_taxon_id":'$spcID',
        "sequencing_type":"'$PROTOCOL'"

    },
    "send_email":false,
    "outputs":[
        {
            "label":"config",
            "file":"params.txt",
            "metadata":{
                "parent_library_name":"'$POOL_ID'"
            }
        },
        {
            "label":"shell_script",
            "file":"run_'$libID'.sh",
            "metadata":{
                "library_name":"'$libID'"
            }
        },
        {
            "label":"logs",
            "file":"'$libID'.logs.tar.gz",
            "metadata":{
                "library_name":"'$libID'"
            }
        },
        {
            "label":"stats",
            "file":"'$libID'.stats",
            "metadata":{
                "library_name":"'$libID'"
            }
        },
        {
            "label":"bam_file",
            "file":"'$libID'.cleaned.bam",
            "metadata":{
                "library_name":"'$libID'",
                "aligner":"STAR",
                "aligner_version":"'$starVersion'"
            }
        },
        {
            "label":"DGE",
            "file":"'$libID'.dge.gz",
            "metadata":{
                "library_name":"'$libID'"
            }
        }
    ]
}' >> $jatDir/metadata.json

echo "
# run the folowing command to archive analysis for this library:
logtext=\$(jat import rnd_rnaseq_singlecell $jatDir/metadata.json)
echo $libID \$logtext >> $(echo $config | sed 's/config$/jat.log/')
"
