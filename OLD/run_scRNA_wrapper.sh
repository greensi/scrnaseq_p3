#!/bin/bash
## Wrapper script to run raw fastq files from a dropseq or 10X experiment through a custom pipeline drawing heavily on:
## 1. the steps described in:
##     Zhang et al 2019, Molecular Cell  doi.org/10.1016/j.molcel.2018.10.020 
## 	and implemented in the python package baseqDrops found at:
##     https://github.com/beiseq/baseqDrops
## 2. the steps described in:
##		Macosko et al 2015, Cell doi.org/10.1016/j.cell.2015.05.002
##	and implemented in the java scripts at:
##      https://github.com/broadinstitute/Drop-seq
## Author: Sharon Greenblum
## Institute: Joint Genome Institute
## Date: July 9, 2019

usage()
{
	echo "usage: run_scRNA_wrapper.sh configFile [steps=all] [time=6:00:00] [cpus=32] [qos=genepool_shared]

	** steps must be one or more (comma-separated) of the following keywords:
	
			genomePrep,linkfastQ,align,clean,dge
	
			or 

			all[default]
	
	"
}

## set default options
mysteps=all
time=6:00:00
cpus=32
qos=genepool_shared

## set paths and tools
currentDir=$PWD
scriptsDir=$(dirname $0)
dstDir=$scriptsDir/drop-seq_v2.3
bbtools="shifter --image=docker:bryce911/bbtools:latest"
STAR="shifter --image=docker:mgibio/star STAR"
samtools="shifter --image=docker:mjblow/samtools-1.9:latest samtools"
mypython=$(which python)
loadjamo="module load jamo"
unloadjamo="module unload jamo"

starVersion="star"$(${STAR} --version | cut -f2 -d'_' | cut -f1-2 -d'.')

## get variables from command line
config=$1
for i in $(echo "$@" | awk '{$1="";print $0}'); do
case $i in
	steps=*)
    	mysteps="${i#*=}"
    	shift 
    ;;
    time=*)
    	time="${i#*=}"
    	shift 
    ;;
    cpus=*)
    	cpus="${i#*=}"
    	shift 
    ;;
    qos=*)
    	qos="${i#*=}"
    	shift 
    ;;
    *)  # unknown option
        echo "unknown command line argument: $i"; usage; exit 1;  
    ;;
esac
done

## read in config file
. $scriptsDir/read_config.sh
echo "############################"
echo "# running scRNA analysis pipeline for pool [ $POOL_ID ]"
echo "# aligning [ $PROTOCOL ] libraries in [ $FASTQ_DIR ]"
echo "# to genomes specified in [ $REF_CONFIG ]"
echo "# and writing analysis output files to [ $LIBS_DIR/$POOL_ID/[library ID]/$NAME ]
############################"

## set up location of wrapper files
genomePrepScript=$(echo $REF_CONFIG | sed 's/.config$/_prepGenomes.sh/'); 
libPrepWrapper=$(echo $config | sed 's/.config$/_runLibraries.sh/')
echo "#!/bin/bash" > $libPrepWrapper
dependString=""

## parse steps
allSteps=linkfastQ,align,clean,dge
if [ $mysteps == "help" ]; then usage; exit 1; fi
if [ $mysteps == all ]; then 
	mysteps="genomePrep,"$allSteps; 
fi
stepString=""
for step in $(echo $allSteps | tr ',' '\n'); do
	stepString=$stepString$( echo $mysteps | grep -c $step)" "
done

#############
### prep genomes
############
if [ $(echo $mysteps | grep genomePrep) ]; then 
	if [ $REF_OVERWRITE != TRUE ] && [ -e $REF_DIR/$refPrefix/$starVersion/STAR/Genome ]; then
		echo "#using previously created ref genome files in $REF_DIR/$refPrefix/$starVersion
		#set REFGENOME_OVERWRITE to TRUE in config file to overwrite instead"
	else
		if [ ! -e $REF_DIR/$refPrefix ]; then mkdir $REF_DIR/$refPrefix; fi
		#rm -r $REF_DIR/$refPrefix/*
		$dstDir/genomePrep.py $REF_CONFIG --s "$STAR" > /dev/null  
		mv prep.sh $genomePrepScript
		chmod +x $genomePrepScript
		echo "mkdir $REF_DIR/$refPrefix/$starVersion; mv $REF_DIR/$refPrefix/STAR $REF_DIR/$refPrefix/$starVersion" >> $genomePrepScript
		echo "
		sID=\$(sbatch --chdir $REF_DIR/$refPrefix -J prepGenome.$refPrefix -q $qos -A gtrnd \
		-t 2:00:00 -N 1 -n 1 -c 6 \
		-o" $(echo $genomePrepScript | sed 's/.sh/.out/') "-e" $(echo $genomePrepScript | sed 's/.sh/.err/') $genomePrepScript " | tr ' ' '\n' | tail -n 1)" >> $libPrepWrapper
		dependString="--dependency=afterok:\$sID"
	fi
fi

if [ $mysteps == genomePrep ]; then chmod +x $libPrepWrapper; echo "
# prep genome by running
$libPrepWrapper
"; exit; fi

#############
### prep libraries
############

## get list of libraries from jamo if not supplied
if [ ! -e $LIBS_META ]; then 

	myfields="metadata.library_name,\
	file_name,\
	metadata.physical_run.dt_sequencing_end,\
	metadata.physical_run.instrument_type,\
	metadata.read_stats.file_num_reads,\
	metadata.rqc.read_qc.bwa_aligned_duplicate_percent,\
	metadata.rqc.read_qc.contam_jgi_contaminants_percent,\
	metadata.rqc.read_qc.contam_rrna_percent,\
	metadata.rqc.read_qc.contam_mitochondrion_percent,\
	metadata.sdm_seq_unit.index_name,\
	metadata.sequencing_project.sequencing_project_name,\
	metadata.sow_segment.sample_name,\
	metadata.sow_segment.sample_isolated_from,\
	metadata.sow_segment.ncbi_tax_id,\
	metadata.sow_segment.species,\
	metadata.sow_segment.strain,
	metadata.sequencing_project.comments"

	echo "samples in pool are:"
	
	eval $loadjamo
	jamo report \select $myfields \
	where \
	metadata.parent_library_name = $POOL_ID,\
	metadata.sow_segment.material_type = RNA,\
	metadata.fastq_type = sdm_normal \
	as csv | \
	sed 's/metadata\.//g' \
	> $LIBS_META
	eval $unloadjamo
	cat $LIBS_META | cut -f1
fi

## go through list	
cat $LIBS_META | sed 's/ //g' | tr '\t' ',' | awk -F ',' -v libField=1 '(NR==1){ 
		header=($0 ~ /#/); 
		for(ii=1;ii<=NF;ii++){
			if($ii == "library"){
				libField=ii;header=1
			}
		}
	}
	(NR>1 || !header ){print $libField}' | while read libID; do
	
	## define command file and write sbatch command to libPrepWrapper
	libCmdsFile=$(dirname $config)/$libID.$NAME
	echo "
	sbatch $dependString -J $POOL_ID.$libID.$NAME -q $qos -A gtrnd -t $time -N 1 -n 1 -c $cpus \
	-o $libCmdsFile.out -e $libCmdsFile.err $libCmdsFile.sh $stepString
	" >> $libPrepWrapper 
	

	## if commands file already exists, you're done.. move to next lib
	if [ -e $libCmdsFile.sh ]; then echo "$libCmdsFile.sh already exists - delete and re-run to edit"; continue; fi

	## otherwise.....
		
	#set up out directory 
	outdir=$LIBS_DIR/$POOL_ID/$libID/$NAME
	if [ ! -e $outdir ]; then mkdir -p $outdir; fi

	eval $loadjamo
	jamoFile=$(jamo report \select metadata.library_name,file_name \
			where metadata.library_name = $libID,metadata.parent_library_name = $POOL_ID,metadata.fastq_type = sdm_normal | \
			awk '{print $2}')
	eval $unloadjamo		
	fastq=$LIBS_DIR/$POOL_ID/$libID.fastq.gz
	bam=$libID.bam
	tmpbam=$libID.tmp.bam
	finalbam=$libID.cleaned.bam
	dge=$libID.dge

	adapterseq=AAGCAGTGGTATCAACGCAGAGTGAATGGG
	adapterlen=$(echo $adapterseq | wc -c | awk '{print ($1 - 1)}')


	### commands
	linkFastq="$loadjamo; jamo link filename $jamoFile 1>&2 ; mv $jamoFile $fastq; $unloadjamo"
	trimR1adapter="$bbtools bbduk.sh in=stdin.fq int=t skipr2=t literal=ACGTN k=1 ktrim=l restrictleft=$TRIM_R1 out=stdout.fq unbgzip=f 2>> trim.report.txt "
	trimR2adapter="$bbtools bbduk.sh in=stdin.fq int=t skipr1=t literal=$adapterseq ktrim=l k=$adapterlen hdist=2 mink=5 hdist2=0 rcomp=f minlength=10 out=stdout.fq unbgzip=f 2>> trim.report.txt"
	trimR2polyA="$bbtools bbduk.sh in=stdin.fq int=t skipr1=t ktrim=r k=6 hdist=0 literal=AAAAAA rcomp=f minlength=10 out=stdout.fq unbgzip=f 2>> trim.report.txt"
	addBCtoReadName="$mypython $scriptsDir/tag_fastq.py -p $PROTOCOL -o /dev/stdout -f /dev/stdin"
	aligncpus=$(echo $cpus | awk '{if($1-6 >1){print $1-6} else{print $1-1}}')
	sortcpus=$(($cpus-$aligncpus))

	inCMD=$(echo " zcat $fastq | " $([ $TRIM_R1 -gt 0 ] && ( echo "$trimR1adapter |") || ( echo "")) "$trimR2adapter | $trimR2polyA | $addBCtoReadName " )
	align=$(echo "echo '#trimming stats' > trim.report.txt; \
	${STAR} \
	--genomeDir $REF_DIR/$refPrefix/$starVersion/STAR \
	--runThreadN $aligncpus \
	--outFileNamePrefix align. \
	--outBAMsortingBinsN 65 \
	--limitBAMsortRAM 11000000000 \
	--outStd BAM_SortedByCoordinate --outSAMtype BAM SortedByCoordinate \
	--readFilesIn <($inCMD)" | tr '\t' ' ')
	 #--outBAMsortingBinsN 100 : this tells star to divide the genome into 100 bins instead of the default 50 for sorting... means it writes more files to disk but uses less memory
	 #--limitBAMsortRAM 11000000000 : this tells star to use 11G of ram to sort the reads after mapping.  the default behavior is to use 6 threads and RAM = the total size of the genome+index (~4G for Bd ~3G for At); instead we tell it to use (almost) all of the RAM in the 6 cpus
	tagCellUMI="$samtools view -h -F 260 -q 10 | awk -F '_' '(\$1 ~ /^@/){print}(\$1 !~ /^@/){print \$0\"\\tXC:Z:\"\$2\"\\tXM:Z:\"\$3}' " #| $samtools sort -m 1G -@ $sortcpus -o - - 
	tagGene="$dstDir/TagReadWithGeneFunction O=/dev/stdout ANNOTATIONS_FILE=$REF_DIR/$refPrefix/$refPrefix.gtf INPUT=/dev/stdin SUMMARY=tag.geneReport.txt"
	geneFilterBam="$samtools view -h - | awk -v outfile=\"tag.geneReport.txt\" 'BEGIN{print \"GENIC\tTOTAL\" >> outfile; genicCt=0; totalCt=0};(/^@/){print};(! /^@/){totalCt++; if(! /XF:Z:INTERGENIC/){print;genicCt++}}; END{print genicCt\"\t\"totalCt >> outfile}' | $samtools view -hb -"
	## keep only genic reads (CODING / UTR / INTRONIC regions) from SENSE strand
	indexBam="$samtools index $bam"
	getBamStats="$dstDir/BamTagHistogram INPUT=$bam TAG=XC O=tag.cellRdCts.gz; $dstDir/BamTagOfTagCounts INPUT=$bam PRIMARY_TAG=XC SECONDARY_TAG=XM O=tag.cellUMICts.gz; join -j1 2 -j2 1 <(zcat tag.cellRdCts.gz | sort -k2,2) <(zcat tag.cellUMICts.gz | cut -f1-2 | sort -k1,1) | sort -k3,3gr | gzip -c > tag.cellRdUMICts.gz"
	
	cleanBam=$(case $PROTOCOL in
		"dropseq" ) echo "$dstDir/DetectBeadSubstitutionErrors INPUT=$bam OUTPUT=$tmpbam NUM_THREADS=$aligncpus TMP_DIR=$outdir MIN_UMIS_PER_CELL=20 OUTPUT_REPORT=clean.subReport.txt; $dstDir/DetectBeadSynthesisErrors INPUT=$tmpbam NUM_THREADS=$aligncpus MIN_UMIS_PER_CELL=20 OUTPUT_STATS=clean.synthStats.txt SUMMARY=clean.synthSummary.txt REPORT=clean.synthReport.txt CREATE_INDEX=true TMP_DIR=$outdir OUTPUT=$finalbam"
		;;
		"10X" ) echo "$mypython $scriptsDir/clean_10x.py -i $bam -o $finalbam -p 10X -c tag.cellRdUMICts.gz -m $MIN_UMIS_PER_CELL -s clean.bcStats.txt --hamming -w $WHITELIST -t $aligncpus > clean.10XReport.txt"
		;;
	esac)
	
	makeDGE="$dstDir/DigitalExpression I=$finalbam O=$dge.gz SUMMARY=dge.summary.txt MIN_NUM_TRANSCRIPTS_PER_CELL=$MIN_UMIS_PER_CELL OUTPUT_LONG_FORMAT=$dge.long.gz"
	

	#write to commands file
	echo "#!/bin/bash" > $libCmdsFile.sh
	echo "export LC_ALL=C.UTF-8; export LANG=C.UTF-8" >> $libCmdsFile.sh
	echo -e 'doLink=$1; doAlign=$2; doClean=$3; doDGE=$4;' >> $libCmdsFile.sh
	echo "cd $outdir"  >> $libCmdsFile.sh
		
	echo "
	
	if [ \$doLink -eq 1 ] ; then  $linkFastq ; fi

	if [ \$doAlign -eq 1 ] ; then  $align | $tagCellUMI | $tagGene | $geneFilterBam > $bam ; $indexBam;  $getBamStats ; fi

	if [ \$doClean -eq 1 ] ; then $cleanBam ; fi

	if [ \$doDGE -eq 1 ] ; then $makeDGE; fi

	
	" >> $libCmdsFile.sh
	
	chmod +x $libCmdsFile.sh

done

chmod +x $libPrepWrapper
echo "
# prep libraries by running
$libPrepWrapper
"