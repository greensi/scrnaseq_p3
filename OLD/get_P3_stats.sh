#!/bin/bash

################
## get stats from dropseq runs
###############

## read in config
config=$1
minUMIsPerStamp=${2:-1000}
. $(dirname $0)/read_P3_config.sh

## for each lib
echo -e '$NAME,$libID,$PROTOCOL,$stamps,$rawReadPairs,$abundantBCs,$abundantBCReads,$aggBCs,$aggBCReads,$bcErrorRate,$starInputReads,$starMappedReads,$unimap,$multimap,$unmap,$geneTagged_20UMI_BCs,$subError_BCs,$polyT_BCs,$synthError_BCs,$otherError_BCs,$stamps,$avgReadsPerStamp,$avgUMIsPerStamp,$avgGenesPerStamp' | sed 's/\$//g' 

cat $LIBS_META | sed 's/ //g' |sed 's/, /,/g' | tr '\t' ',' | awk -F ',' -v libField=1 '
(NR==1){header=($0 ~ /#/);for(ii=1;ii<=NF;ii++){
	if($ii == "library"){libField=ii;header=1 }
}}
(NR>1 || !header){print $libField}' | \
while read libID; do

myDir=$LIBS_DIR/$LIBS_POOL/$libID/$NAME


fastq=$(ls $LIBS_DIR/$LIBS_POOL/$libID | grep fastq.gz$ | grep -v R1 | grep -v R2)
errFile=$(dirname $config)/$libID.stepcountstatssplitstar.err
R1=$LIBS_DIR/$LIBS_POOL/$libID/$libID.R1.fastq.gz
R2=$LIBS_DIR/$LIBS_POOL/$libID/$libID.R2.fastq.gz
R1pretrim=$(echo $R1 | sed 's/fastq.gz/pretrim.fastq.gz/')
R2pretrim=$(echo $R2 | sed 's/fastq.gz/pretrim.fastq.gz/')
counts=$myDir/bcCts.txt
stats=$myDir/bcStats.txt
reads=$myDir/bcReads.fq
bam=$myDir/star_aligned.bam
starlog=$myDir/starLog.final.out
genebam=$myDir/gene_tagged.bam
funcbam=$myDir/function_tagged.bam
subbam=$myDir/substitution_repaired.bam
subreport=$myDir/substitution_error_report.txt
synthstats=$myDir/synthesis_error_stats.txt
synthsummary=$myDir/synthesis_error_summary.txt
synthreport=$myDir/synthesis_error_report.txt
finalbam=$myDir/final.bam
dge=$myDir/$libID.dge.gz
dgesummary=$myDir/dge_summary.txt
dgelong=$myDir/$libID.dge_long.gz
	


## Read stats
## raw read count (R1 + R2)
rawReadPairs=$(zcat $R1 | grep -c "^@")

## barcodes with at least 50 reads
abundantBCs=$(tail -n +2 $counts | wc -l)
abundantBCReads=$(tail -n +2 $counts | awk -F ',' '{total=total+$2} END{print total}')

## merge bcs within hamming dist of 1, remove non-whitelist bcs, select up to 5000 bcs with at least 1000 reads
aggBCs=$(tail -n +2 $stats | wc -l)
aggBCReads=$(tail -n +2 $stats | awk -F ',' '{total=total + $(NF-1)} END{print total}')
bcErrorRate=$(tail -n +2 $stats | awk -F ',' -v aggBCReads="$aggBCReads" '{total=total + $3} END{print total/aggBCReads}')

### RQC (if available)
filtered=$(curl -sL https://rqc.jgi-psf.org/fullreport/report/${fastq} | grep -A 1 "<th align=\"left\">Filtered \%</th>" | tail -n 1 | cut -f2 -d">" | cut -f1 -d "<")
rRNA=$(curl -sL https://rqc.jgi-psf.org/fullreport/report/${fastq} | grep "<td>Ribosomal RNA</td>" | cut -f7 -d'>' | cut -f1 -d'<')
adapter=$(curl -sL https://rqc.jgi-psf.org/fullreport/report/${fastq} | grep "<td>Adapter</td>" | cut -f7 -d'>' | cut -f1 -d'<')

## MAPPING QC
starInputReads=$(grep "Number of input reads" $starlog | cut -f2 | awk '{total=total + $1} END {print total}')
starMappedReads=$(samtools flagstat $finalbam | head -2 | awk '(NR==1){total=$1}(NR==2){print total-$1}')
#starMappedReads=NA
unimap=$(grep "Uniquely mapped reads number" $starlog  | cut -f2 | awk -v inputNum="$starInputReads" '{total=total + $1} END {print total/inputNum}')
multimap=$(grep "Number of reads mapped to multiple loci" $starlog  | cut -f2 | awk -v inputNum="$starInputReads" '{total=total + $1} END {print total/inputNum}')
unmap=$(echo $unimap,multimap | awk -F ',' '{print 1-$1-$2}')

## Barcode stats
if [ $PROTOCOL == "dropseq" ]; then
geneTagged_20UMI_BCs=$(grep "# TOTAL_BARCODES_TESTED=" $subreport | cut -f2 -d'=')
subError_BCs=$(grep "# BARCODES_COLLAPSED=" $subreport | cut -f2 -d'=')
polyT_BCs=$(grep "# POLY_T_BIASED_BARCODES=" $subreport | cut -f2 -d'=')
synthError_BCs=$( cat $synthsummary | grep '^[0-9]' | head -1 | awk '{print $3}')
otherError_BCs=$( cat $synthsummary | grep '^[0-9]' | head -1 | awk '{print $4+$5+$6+$7}')
else 
	geneTagged_20UMI_BCs=NA; subError_BCs=NA; polyT_BCs=NA; synthError_BCs=NA; otherError_BCs=NA
fi

## nof STAMPS 
stampStats=$(grep -v "^#" $dgesummary | tail -n +3 | awk -v minUMI="$minUMIsPerStamp" '($3>=minUMI){
	stamps=stamps+1; reads=reads+$2; umis=umis+$3; genes=genes+$4
} END {print stamps" "reads/stamps" "umis/stamps" "genes/stamps}')
stamps=$(echo $stampStats | cut -f1 -d' ')
avgReadsPerStamp=$(echo $stampStats | cut -f2 -d' ')
avgUMIsPerStamp=$(echo $stampStats | cut -f3 -d' ')
avgGenesPerStamp=$(echo $stampStats | cut -f4 -d' ')

echo $NAME,$libID,$PROTOCOL,$stamps,$rawReadPairs,$abundantBCs,$abundantBCReads,$aggBCs,$aggBCReads,$bcErrorRate,$starInputReads,$starMappedReads,$unimap,$multimap,$unmap,$geneTagged_20UMI_BCs,$subError_BCs,$polyT_BCs,$synthError_BCs,$otherError_BCs,$stamps,$avgReadsPerStamp,$avgUMIsPerStamp,$avgGenesPerStamp
done