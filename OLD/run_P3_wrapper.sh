#!/bin/bash

## Wrapper script to run raw fastq files from a dropseq,inDrop,or10X experiment through a custom pipeline drawing heavily on:
## 1. the steps described in:
##     Zhang et al 2019, Molecular Cell  doi.org/10.1016/j.molcel.2018.10.020 
## 	and implemented in the python package baseqDrops found at:
##     https://github.com/beiseq/baseqDrops
## 2. the steps described in:
##		Macosko et al 2015, Cell doi.org/10.1016/j.cell.2015.05.002
##	and implemented in the java scripts at:
##      https://github.com/broadinstitute/Drop-seq
## Author: Sharon Greenblum
## Institute: Joint Genome Institute
## Date: June 20, 2019

usage()
{
	echo "usage: run_P3_wrapper.sh configFile [steps=steps] [time=time] [cpus=cpus] [qos=qos]

	** steps must be one or more (comma-separated) of the following keywords:
	
			genomePrep,getfastQ,deinterleave,trimR1,count,stats,fastq,align,tag,clean,dge
	
			or 

			all[default]
	
	** note that these scripts may require python 3 [not tested]

	"
}

## set default options
mysteps=all
time=6:00:00
cpus=32
qos=genepool_shared

## set default dirs
currentDir=$PWD
thisScriptDir=`dirname "$0"`
bqDir=$thisScriptDir/my_baseqDrops
dstDir=$thisScriptDir/drop-seq_v2.3

## set paths and tools
bbmap="shifter --image=docker:bryce911/bbtools:latest"
STAR="shifter --image=docker:vrsingan/star:v2.5.4a STAR"
samtools="shifter --image=docker:mjblow/samtools-1.9:latest samtools"
cutadapt="shifter --image=docker:mjblow/cutadapt:1.8.3 cutadapt"


## get variables from command line
config=$1
for i in $(echo "$@" | awk '{$1="";print $0}'); do
case $i in
	steps=*)
    	mysteps="${i#*=}"
    	shift 
    ;;
    trimR1=*)
    	trimR1="${i#*=}"
    	shift 
    ;;
    time=*)
    	time="${i#*=}"
    	shift 
    ;;
    cpus=*)
    	cpus="${i#*=}"
    	shift 
    ;;
    qos=*)
    	qos="${i#*=}"
    	shift 
    ;;
    *)  # unknown option
        echo "unknown argument: $i"; usage; exit 1;  
    ;;
esac
done

## read in config file
. $thisScriptDir/read_P3_config.sh
echo "############################"
echo "# running baseqDrops pipeline for pool [ $LIBS_POOL ]"
echo "# aligning [ $PROTOCOL ] libraries in [ $LIBS_META ]"
echo "# to genomes specified in [ $REF_CONFIG ]"
echo "# and writing files to [ $LIBS_DIR ]
############################"

## set up location of wrapper files
genomePrepWrapper=$(echo $REF_CONFIG | sed 's/.config$/_dropseqPrep.sh/')
libPrepWrapper=$(echo $config | sed 's/.config$/.libPrep.sh/')

## parse steps
if [ $mysteps == "help" ]; then usage; exit 1; fi
if [ $mysteps == all ]; then 
	mysteps=genomePrep,getfastQ,deinterleave,trimR1,count,stats,fastq,align,tag,clean,dge; 
fi


#############
### prep genomes
############
if [ $(echo $mysteps | grep genomePrep) ]; then 
	if [ $REF_OVERWRITE != TRUE ] && [ -e $REF_DIR/$refPrefix/STAR/Genome ]; then
		echo "#using previously created ref genome files in $REF_DIR/$refPrefix
		#set REFGENOME_OVERWRITE to TRUE in config file to overwrite instead"
	else
		if [ ! -e $REF_DIR/$refPrefix ]; then mkdir $REF_DIR/$refPrefix; fi
		#rm -r $REF_DIR/$refPrefix/*
		$dstDir/genomePrep.py $REF_CONFIG > /dev/null  
		mv prep.sh $genomePrepWrapper
		chmod +x $genomePrepWrapper
		echo "#prep reference genome by running: 
		sbatch --chdir $REF_DIR/$refPrefix -J dropseq.prepGenome_$refPrefix -q genepool_shared -A gtrnd \
		-t 2:00:00 -N 1 -n 1 -c 6 \
		-o" $(echo $genomePrepWrapper | sed 's/.sh/.out/') "-e" $(echo $genomePrepWrapper | sed 's/.sh/.err/') $genomePrepWrapper
	fi
fi

if [ $mysteps == genomePrep ]; then exit; fi

#############
### prep libraries
############

## define lib steps
stepString=""
allSteps=getfastQ,deinterleave,trimR1,count,stats,fastq,align,tag,clean,dge
for step in $(echo $allSteps | tr ',' '\n'); do
	stepString=$stepString$( echo $mysteps | grep -c $step)" "
done

## get list of libraries from jamo if not supplied
if [ ! -e $LIBS_META ]; then 
	jamo report select metadata.library_name where metadata.parent_library_name = $LIBS_POOL,metadata.fastq_type = sdm_normal > $LIBS_META
fi

## go through list	
echo "#!/bin/sh" > $libPrepWrapper
cat $LIBS_META | sed 's/ //g' | sed 's/, /,/g' | tr '\t' ',' | awk -F ',' -v libField=1 '(NR==1){ 
		header=($0 ~ /#/); 
		for(ii=1;ii<=NF;ii++){
			if($ii == "library"){
				libField=ii;header=1
			}
		}
	}
	(NR>1 || !header ){print $libField}' | while read libID; do
	
	## define command file and sbatch command to libPrepWrapper
	libCmdsFile=$(dirname $config)/$libID.$NAME
	echo sbatch -J p3.$libID -q $qos -A gtrnd -t $time -N 1 -n 1 $(if [ $qos == "genepool_shared" ]; then echo "-c $cpus"; fi) -o $libCmdsFile.out -e $libCmdsFile.err $libCmdsFile.sh $stepString >> $libPrepWrapper 


	## if commands file already exists, you're done.. move to next lib
	if [ -e $libCmdsFile ]; then echo $libCmdsFile; continue; fi

	## otherwise.....
		
	#set up out directory 
	outdir=$LIBS_DIR/$LIBS_POOL/$libID
	if [ ! -e $outdir/$NAME ]; then mkdir -p $outdir/$NAME; fi

	
	# set up file names
	fastq=$outdir/$(jamo report \select metadata.library_name,file_name \
		where metadata.library_name = $libID,metadata.parent_library_name = $LIBS_POOL,metadata.fastq_type = sdm_normal | \
		awk '{print $2}')
	R1=$outdir/$libID.R1.fastq.gz
	R2=$outdir/$libID.R2.fastq.gz
	R1pretrim=$(echo $R1 | sed 's/fastq.gz/pretrim.fastq.gz/')
	R2pretrim=$(echo $R2 | sed 's/fastq.gz/pretrim.fastq.gz/')
	counts=$outdir/$NAME/bcCts.txt
	stats=$outdir/$NAME/bcStats.txt
	reads=$outdir/$NAME/bcReads.fq
	bam=$outdir/$NAME/star_aligned.bam
	genebam=$outdir/$NAME/gene_tagged.bam
	funcbam=$outdir/$NAME/function_tagged.bam
	subbam=$outdir/$NAME/substitution_repaired.bam
	subreport=$outdir/$NAME/substitution_error_report.txt
	synthstats=$outdir/$NAME/synthesis_error_stats.txt
	synthsummary=$outdir/$NAME/synthesis_error_summary.txt
	synthreport=$outdir/$NAME/synthesis_error_report.txt
	finalbam=$outdir/$NAME/final.bam
	dge=$outdir/$NAME/$libID.dge.gz
	dgesummary=$outdir/$NAME/dge_summary.txt
	dgelong=$outdir/$NAME/$libID.dge_long.gz
	
	#set up commands file
	echo "#!/bin/sh" > $libCmdsFile.sh
	echo "export LC_ALL=C.UTF-8; export LANG=C.UTF-8" >> $libCmdsFile.sh
	echo -e 'doLink=$1; doDeint=$2; doTrimR1=$3; doCount=$4; doAgg=$5; doFastq=$6; doAlign=$7; doTag=$8; doClean=$9; doDGE=$10' >> $libCmdsFile.sh
	echo "mill_reads=1000;" >> $libCmdsFile.sh
	
	

	## link fastq from jamo
	echo "
	######LinkFastq###########
	if [ \$doLink -gt 0 ]; then
		echo linking $fastq
		cd $outdir; jamo link filename $(basename $fastq) 1>&2 ; cd $currentDir
	fi
	" >> $libCmdsFile.sh
	
	## deinterleave 
	echo "
	#######Deinterleave##########
	if [ \$doDeint -gt 0 ]; then
		if [ ! -e $fastq ]; then echo $fastq not found; exit 1; fi
		echo deinterleaving $fastq
		$bbtools reformat.sh in=$fastq \
		int=t overwrite=t \
		out1=$R1 \
		out2=$R2;
	fi
	" >> $libCmdsFile.sh

	## trim R1
	echo "
	########Trim R1#########
	if [ \$doTrimR1 -gt 0 ] && [ $TRIM_R1 -gt 0 ]; then
		if [ ! -e $R1pretrim ]; then 
			if [ ! -e $R1 ]; then echo $R1 not found; exit 1; fi; 
			mv $R1 $R1pretrim; 
		fi; 
		if [ ! -e $R2pretrim ]; then 
			if [ ! -e $R2 ]; then echo $R2 not found; exit 1; fi; 
			mv $R2 $R2pretrim; 
		fi; 
		$cutadapt -u $TRIM_R1 \
		-m 25 \
		-o $R1 \
		-p $R2 \
		$R1pretrim \
		$R2pretrim
	fi
	" >> $libCmdsFile.sh

	### count reads per barcode
	echo "
	if [ \$doCount -gt 0 ]; then
		python $bqDir/do_baseqDrops_counting.py $R1 $counts $PROTOCOL $MIN_READS_CT \$mill_reads
	fi
	" >> $libCmdsFile.sh

	## aggregate and filter barcodes
	echo "
	if [ \$doAgg -gt 0 ]; then
		python $bqDir/do_baseqDrops_aggregating.py $PROTOCOL $counts $MIN_READS_AGG $stats $HAMMING $WHITELIST
	fi
	" >> $libCmdsFile.sh

	### add barcodes + UMIs to read headers and write fastq for valid barcode reads
	echo "
	if [ \$doFastq -gt 0 ]; then
		python $bqDir/do_baseqDrops_print_fastq.py $PROTOCOL $stats $R1 $R2 $reads \$mill_reads
	fi
	" >> $libCmdsFile.sh

	### map fastq and add cell and UMI tags
	echo "
	if [ \$doAlign -gt 0 ]; then
		${STAR} \
		--genomeDir $REF_DIR/$refPrefix/STAR \
		--readFilesIn $reads \
		--runThreadN $cpus \
		--outFileNamePrefix $outdir/$NAME/star \
		--outStd BAM_Unsorted --outSAMtype BAM Unsorted | \
		${samtools} sort -n -@ $cpus - | \
		$samtools view -h -F4 | \
		awk -F '_' '(\$1 ~ /^@/){print}(\$1 !~ /^@/){print \$0\"\tXC:Z:\"\$2\"\tXM:Z:\"\$3}' | \
		$samtools sort -o $bam -O bam -@ $cpus - ;
	fi
	" >> $libCmdsFile.sh

	## tag reads with gene using dropseq tools
	echo "
	if [ \$doTag -gt 0 ]; then
		$dstDir/TagReadWithInterval I=$bam O=$genebam TMP_DIR=$outdir INTERVALS=$REF_DIR/$refPrefix/$refPrefix.genes.intervals TAG=XG;
		$dstDir/TagReadWithGeneFunction O=$funcbam ANNOTATIONS_FILE=$REF_DIR/$refPrefix/$refPrefix.refFlat INPUT=$genebam; 
	fi
	" >> $libCmdsFile.sh

	## if dropseq, do bead cleanup
	if [ $PROTOCOL == "dropseq" ]; then
		echo "
		if [ \$doClean -gt 0 ]; then
			$dstDir/DetectBeadSubstitutionErrors INPUT=$funcbam OUTPUT=$subbam TMP_DIR=$myDir MIN_UMIS_PER_CELL=20 OUTPUT_REPORT=$subreport; 
			$dstDir/DetectBeadSynthesisErrors INPUT=$subbam MIN_UMIS_PER_CELL=20 OUTPUT_STATS=$synthstats SUMMARY=$synthsummary REPORT=$synthreport CREATE_INDEX=true TMP_DIR=$outdir OUTPUT=$finalbam; 
		fi
		" >> $libCmdsFile.sh
	else 
		echo "
		if [ \$doClean -gt 0 ]; then
			mv $funcbam $finalbam; 
		fi
		" >> $libCmdsFile.sh
	fi

	## make DGE
	echo "
	if [ \$doDGE -gt 0 ]; then
		$dstDir/DigitalExpression I=$finalbam 	O=$dge 	SUMMARY=$dgesummary MIN_NUM_TRANSCRIPTS_PER_CELL=1000 OUTPUT_LONG_FORMAT=$dgelong
		rm $bam $genebam $funcbam $subbam $reads
	fi
	" >> $libCmdsFile.sh


	chmod +x $libCmdsFile.sh

done

echo "
# prep libraries by running
$libPrepWrapper
"


