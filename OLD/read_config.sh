#!/bin/bash 

usage()
{
	echo "usage: 
	# . read_config.sh
	## the variable 'config' must already be defined
	"
}
#######
## script to read in config file for single cell RNA seq analysis pipeline
## **required parameters are:
##      PROTOCOL : (10X|dropseq)
##		FASTQ_DIR : fastq files should be [linked to]/[in] $FASTQ_DIR
##		LIBS_LIST : filename for list of library IDs to analyze; if file doesn't exist, it will be created and all RNA libraries with $POOL_ID in jamo or all fastq files in $FASTQ_DIR will be analyzed
##		REF_DIR : directory with prepped genome files
##		OUT_DIR : parent directory of output files 
## **optional parameters are:
##		POOL_ID : pool ID in jamo, not needed if fastq files have already been downloaded/linked (default none); 
##		INTERLEAVED : whether or not fastq files are interleaved; if not, expects file names to have format '[samplename]*R1*.fastq' and '[samplename]*R1*.fastq' (default TRUE)
##		TRIM_R1 : how many leading bp of R1 to trim; can be any value from 0-130 (default: 0)
##		WHITELIST : filter barcodes to those in this file (default: none)
##		HAMMING(TRUE|FALSE) : aggregate barcodes within 1 hamming distance (default: TRUE)
##		MIN_UMIS_PER_CELL : minimum nof UMIs per barcode to include in the DGE (default: 1000)
#########

## validate config file
if [ -z $config ]; then echo "must supply config file."; usage; exit 1; fi
if [ $config == "help" ]; then usage; exit 1; fi
if [ ! -e $config ]; then echo "config file $config does not exist"; exit 1; fi

## set defaults
POOL_ID=""
INTERLEAVED=TRUE
TRIM_R1=0
WHITELIST=""
HAMMING=TRUE
MIN_UMIS_PER_CELL=1000

## read in config
source $config; 

## check for required args
if [ $PROTOCOL != "dropseq" ] && [ $PROTOCOL != "10X" ]; then echo "must specify PROTOCOL - either dropseq or 10X"; exit 1; fi
if [ -z $FASTQ_DIR ]; then echo "must supply valid FASTQ_DIR - the location of fastq files"; exit 1; fi
if [ -z $REF_DIR ] || [ ! -e $REF_DIR ]; then echo "must supply valid REF_DIR - the location of prepped reference genome files"; exit 1; fi
if [ -z $OUT_DIR ]; then echo "must supply OUT_DIR - the parent directory for output files"; exit 1; fi
if [ -z $LIBS_LIST ]; then echo "must supply LIBS_LIST - the filename for a list of libraries to analyze (may not exist yet)"; exit 1; fi

## validate / reformat args

if [ ! -e $(dirname $LIBS_LIST) ]; then echo "LIBS_LIST $LIBS_LIST must be located in an existing directory"; exit 1; fi

if [ ! -z $WHITELIST ] && [ ! -e $WHITELIST ]; then 
	echo "whitelist $WHITELIST is invalid." exit 1; 
fi

case  1:${TRIM_R1:--} in 
	(1:*[!0-9]*)  echo "invalid entry for trimR1; must be a number 0-130"; exit 1; ;; 
	($((TRIM_R1>130))*) echo "invalid entry for trimR1;  must be a number 0-130"; exit 1; ;; 
esac

INTERLEAVED=$(echo $INTERLEAVED | tr a-z A-Z )
if [ ! $INTERLEAVED == "TRUE" ] && [ ! $INTERLEAVED == "FALSE" ]; then 
	echo "INTERLEAVED arg is invalid; must be TRUE or FALSE" exit 1; 
fi

HAMMING=$(echo $HAMMING | tr a-z A-Z )
if [ ! $HAMMING == "TRUE" ] && [ ! $HAMMING == "FALSE" ]; then 
	echo "HAMMING arg is invalid; must be TRUE or FALSE" exit 1; 
fi

## check if ref dir contains the right files
if [ ! ls $REF_DIR/*.fasta.gz ] && [ ! ls $REF_DIR/*.gtf ]; then 
        echo "$REF_DIR must contain at minimum a fasta.gz file and a gtf file; run prep_ref_data.sh" exit 1;
fi


## extract ref prefix
refPrefix=$(basename $REF_DIR/*.fasta.gz | sed 's/.fasta.gz//')

## create fastq dir if doesnt exist
if [ ! -e $FASTQ_DIR ]; then mkdir -p $FASTQ_DIR; fi

## fill in LIBS_LIST if it doesn't exist yet
if [ ! -e $LIBS_LIST ]; then
	if [ ! -z $POOL_ID ]; then
		echo "LIBS_LIST $LIBS_LIST does not exist - fetching all RNA libraries for pool $POOL_ID"

myfields="metadata.library_name,\
file_name,\
metadata.physical_run.dt_sequencing_end,\
metadata.physical_run.instrument_type,\
metadata.read_stats.file_num_reads,\
metadata.rqc.read_qc.bwa_aligned_duplicate_percent,\
metadata.rqc.read_qc.contam_jgi_contaminants_percent,\
metadata.rqc.read_qc.contam_rrna_percent,\
metadata.rqc.read_qc.contam_mitochondrion_percent,\
metadata.sdm_seq_unit.index_name,\
metadata.sequencing_project.sequencing_project_name,\
metadata.sow_segment.sample_name,\
metadata.sow_segment.sample_isolated_from,\
metadata.sow_segment.ncbi_tax_id,\
metadata.sow_segment.species,\
metadata.sow_segment.strain,
metadata.sequencing_project.comments"
		
jamo report \select $myfields \
where \
metadata.parent_library_name = $POOL_ID,\
metadata.sow_segment.material_type = RNA,\
metadata.fastq_type = sdm_normal \
as csv | \
sed 's/metadata\.//g' \
> $LIBS_LIST
	else
		echo "LIBS_LIST $LIBS_LIST does not exist - running all fastqs in $FASTQ_DIR"
		echo "library_name,filename" > $LIBS_LIST 
		if [ $INTERLEAVED == "TRUE" ]; then
			for fastqFile in $(ls $FASTQ_DIR/*.f*q*); do echo $(basename $fastqFile | sed 's/.f.*q.*$//')","$fastqFile >> $LIBS_LIST; done
		else
			for fastqFile in $(ls $FASTQ_DIR/*.f*q* | sed 's/R[12]/R<*>/' | sort | uniq); do echo $(basename $fastqFile | sed 's/.R<\*>.f.*q.*$//')","$fastqFile | sed 's/<//' | sed 's/>//'  >> $LIBS_LIST; done
		fi
	fi
fi

## go through LIBS_LIST; 
## start by assuming library names are in first column
## if first line starts with a '#', consider it a header
## if first line has a field 'library' or 'library_name', consider it a header and update library column number

libIDs=$(cat $LIBS_LIST | sed 's/ //g' | tr '\t' ',' | awk -F ',' -v libField=1 '(NR==1){ 
		header=($0 ~ /#/); 
		for(ii=1;ii<=NF;ii++){
			if( $ii == "library" || $ii == "library_name" ){
				libField=ii;header=1
			}
		}
	}
	(NR>1 || !header ){print $libField}' | tr '\n' ',')

