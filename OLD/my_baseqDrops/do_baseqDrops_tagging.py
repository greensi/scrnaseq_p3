import os, bisect, sys, json
import re
import numpy as np
import baseqDrops
from baseqDrops.pipeline import pipeline
print(baseqDrops.__file__)


### get command line arguments
genome=sys.argv[1]
pool=sys.argv[2]
samp=sys.argv[3]
step=sys.argv[5]
print("The name of the sample is: " , samp)
print("Running step: " , step)

## set up default directories
datadir="/global/cscratch1/sd/greensi/seqData"
configdir="/global/homes/g/greensi/configs/baseqDrops"


## set up baseq parameters
config=configdir+"/baseqDrops."+genome+".config"
name="baseqDrops"
protocol="10X"
cells=5000
minreads=1000
outdir=datadir+"/"+pool+"/"+samp
fq1=outdir+"/"+samp+".R1.fastq.gz"
fq2=outdir+"/"+samp+".R2.fastq.gz"
top_million_reads=1000
#step="tagging"
parallel=1


## run baseqDrops pipeline
pipeline(config, genome, protocol, int(cells), int(minreads), name, fq1, fq2, outdir, 
top_million_reads, step, parallel)

