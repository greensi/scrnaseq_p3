import os, bisect, sys, json
import re
import numpy as np
import baseqDrops
import baseqDrops.barcode
from baseqDrops.barcode.correct import valid_barcode
from baseqDrops.barcode.correct import get_bool

### get command line arguments
protocol=sys.argv[1]
bc_counts=sys.argv[2]
min_reads=int(sys.argv[3])
bc_stats=sys.argv[4]
doHamming=get_bool(sys.argv[5],"doHamming")
if len(sys.argv) > 7:
	white_path=sys.argv[7]
else:
	white_path=None
if len(sys.argv) > 8:
  white_path2=sys.argv[8]
else:
  white_path2=None

    
print("Examining barcodes in: " , bc_counts)
if doHamming:
	print("--Aggregating barcodes within 1 hamming distance")
if white_path is not None:
	print("--Keeping barcodes on the whitelist ", white_path)
print("--Filtering out barcodes with less than ", min_reads , " reads")
print("Writing results to: " , bc_stats)

## run baseqDrops pipeline
valid_barcode(protocol, bc_counts, min_reads, bc_stats, doHamming, white_path, white_path2)
    