import os, bisect, sys, json
import re
import numpy as np
import baseqDrops
import baseqDrops.barcode
#from baseqDrops.pipeline import pipeline
from baseqDrops.barcode.split_fast import print_fastq_with_barcodes

### get command line arguments
protocol=sys.argv[1]
bc_stats=sys.argv[2]
fq1=sys.argv[3]
fq2=sys.argv[4]
output=sys.argv[5]
if len(sys.argv)>6:
	top_million_reads=int(sys.argv[6])
else:
	top_million_reads=1000

print("Printing reads from barcodes in: " , bc_stats)
print("--Printing max : " , top_million_reads, " million reads")
print("Writing results to: " , output)

## run baseqDrops pipeline
print_fastq_with_barcodes(protocol, bc_stats, fq1, fq2, output,top_million_reads)