import os, bisect, sys, json
import re
import numpy as np
import baseqDrops
import baseqDrops.barcode
#from baseqDrops.pipeline import pipeline
from baseqDrops.barcode.count import count_barcode

### get command line arguments
R2=sys.argv[1]
bc_counts=sys.argv[2]
protocol=sys.argv[3]
min_reads=int(sys.argv[4])
topreads=int(sys.argv[5])

print("Counting barcode reads in: " , R2)
print("--Looking at max : " , topreads, " million reads")
print("--Reporting barcodes with min: " , min_reads , " reads")
print("Writing results to: " , bc_counts)

## run baseqDrops pipeline
count_barcode(R2, bc_counts, protocol, min_reads, topreads)
