import os, sys
from ..config import get_config

def rev_comp(seq):
    ___tbl = {'A': 'T', 'T': 'A', 'C': 'G', 'G': 'C', 'N': 'N'}
    return ''.join(___tbl[s] for s in seq[::-1])

def mutate_single_base(seq):
    mutated = []
    bases = ['A', 'T', 'C', 'G']
    for index in range(0, len(seq)):
        temp = list(seq)
        base_raw = temp[index]
        for base in bases:
            if base != base_raw:
                temp[index] = base
                mutated.append(''.join(temp))
    return mutated

def read_whitelist(protocol, white_path, white_path2="NULL"):
    """
    Read Whitelist, get whitelist from config file: Drops/whitelistDir,
    Return: A set of whitelist barcodes
    """
    if not os.path.exists(white_path):
        sys.exit("WhiteList path does not exist: {}".format(white_path))
    print("[info] The white list is {}".format(white_path)) 
    if protocol == "indrop":
        bc_white = [set(), set()]
        with open(white_path, 'r') as infile:
            for line in infile:
                bc_rev = rev_comp(line.strip())
                bc_white[0].add(bc_rev)
        with open(white_path2, 'r') as infile:
            for line in infile:
                bc_rev = rev_comp(line.strip())
                bc_white[1].add(bc_rev)
    else:
        bc_white = [set()]
        with open(white_path, 'r') as infile:
            for line in infile:
                bc_white[0].add(line.strip())

    return bc_white

def check_whitelist(bc_white, protocol, barcode):
    """
    Check whitelist.

    Usage:
    ::
        check_whitelist(bc_white, "10X", "ATTATATATT")
    """
    if protocol == "indrop":
        if barcode[0:-8] in bc_white[0] and barcode[-8:] in bc_white[1]:
            return 1
        else:
            return 0
    else:
        if barcode in bc_white[0]:
            return 1
        else:
            return 0