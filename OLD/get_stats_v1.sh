#!/bin/bash

################
## get stats from dropseq runs
###############

## read in config
config=$1
p4Dir=`dirname "$0"`
dstDir=$p4Dir/drop-seq_v2.3
. $p4Dir/read_config.sh

## set up output files
statsFile=$(echo $config | sed 's/.config$/.stats/'); 
topBCsFile=$(echo $config | sed 's/.config$/.topBCs/')

if [ $PROTOCOL == "dropseq" ]; then
	echo -e 'pool,library,sampleName,protocol,,$rawReadPairs,$QCfilteredReads,QCrate,,$unimap,$multimap,$unmap,,mapped_BCs,mapped_UMIs,mapped_Reads,$dupRate,,$geneTagged_BCs,$abundant_BCs,$subErrorCollapsed_BCs,$subErrorDropped_BCs,$synthErrorCollapsed_BCs,$synthErrorDropped_BCs,BC_error_rate,corrected_BCs,$stamps('$MIN_UMIS_PER_CELL' UMIs),,$avgReadsPerStamp,$avgUMIsPerStamp,$avgGenesPerStamp' | sed 's/\$//g' > $statsFile
else
	echo -e 'pool,library,sampleName,protocol,,$rawReadPairs,$QCfilteredReads,QCrate,,$unimap,$multimap,$unmap,mapped_Reads,,geneTagged_BCs,geneTagged_UMIs,geneTagged_Reads,geneTagged_Rate,reads_per_UMI,,$whitelist_BCs,$subErrorCollapsed_BCs,$abundant_BCs,abundant_Reads,,$stamps('$MIN_UMIS_PER_CELL' UMIs),$avgReadsPerStamp,$avgUMIsPerStamp,$avgGenesPerStamp' | sed 's/\$//g' > $statsFile
fi

echo "library,barcode,reads,UMIs,genes,readsPerUMI" > $topBCsFile

## for each lib
cat $LIBS_META | sed 's/ //g' |sed 's/, /,/g' | tr '\t' ',' | awk -F ',' -v libField=1 '
(NR==1){header=($0 ~ /#/);for(ii=1;ii<=NF;ii++){
	if($ii == "library"){libField=ii;header=1 }
}}
(NR>1 || !header){print $libField}' | \
while read libID; do

myDir=$LIBS_DIR/$LIBS_POOL/$libID/$NAME
cd $myDir


## data files
fastq=../$libID.fastq.gz
bam=$libID.bam
cleanbam=$libID.cleaned.bam
dge=$libID.dge.gz

## reports
errFile=$(dirname $config)/$libID.p4.err
trimR=trim.report.txt
alignR=align.Log.final.out
tagR=tag.cellRdUMICts.gz
clean10xR=clean.10XReport.txt
subR=clean.subReport.txt
synthR=clean.synthSummary.txt
dgeR=dge.summary.txt

	


## Read stats
## raw read count (R1 + R2)
rawReadPairs=$(grep Input: $trimR | head -1 | awk '{print $2/2}')

### RQC (if available)
#jamoFile=$(basename $(ls -l $fastq | cut -f2 -d'>' | sed 's/ //'))
#filtered=$(curl -sL https://rqc.jgi-psf.org/fullreport/report/${jamoFile} | grep -A 1 "<th align=\"left\">Filtered \%</th>" | tail -n 1 | cut -f2 -d">" | cut -f1 -d "<")
#rRNA=$(curl -sL https://rqc.jgi-psf.org/fullreport/report/${jamoFile} | grep "<td>Ribosomal RNA</td>" | cut -f7 -d'>' | cut -f1 -d'<')
#adapter=$(curl -sL https://rqc.jgi-psf.org/fullreport/report/${jamoFile} | grep "<td>Adapter</td>" | cut -f7 -d'>' | cut -f1 -d'<')

## trim
QCfilteredReads=$(grep "Number of input reads" $alignR | cut -f2 | awk '{total=total + $1} END {print total}')
QCrate=$(echo $QCfilteredReads","$rawReadPairs | awk -F ',' '{print $1/$2}')

## align
unimap=$(grep "Uniquely mapped reads %" $alignR | cut -f2 )
multimap=$(grep "% of reads mapped to multiple loci" $alignR  | cut -f2 )
unmap=$(grep "% of reads unmapped: too short" $alignR | cut -f2 )
mapped_Reads=$(grep -e "Uniquely mapped reads number" -e "Number of reads mapped to multiple loci" $alignR | cut -f2 | awk '{total=total + $1} END {print total}')

## tag
taggedStats=$(zcat $tagR | grep -v "#" | awk '{bcs=bcs+1;reads=reads+$2; umis=umis+$3} END{print bcs","umis","reads}')
geneTagged_Rate=$(echo $mapped_Reads","$taggedStats | awk -F ',' '{print $4/$1}')
reads_per_UMI=$(echo $taggedStats | awk -F ',' '{print $3/$2}')

## clean 
if [ $PROTOCOL == "dropseq" ]; then
	abundant_BCs=$(grep "Starting Barcode Collapse" $errFile | cut -f2 -d'[' | cut -f1 -d']')
	subErrorCollapsed_BCs=$(grep "# BARCODES_COLLAPSED=" $subR | cut -f2 -d'=')
	synthErrorCollapsed_BCs=$( cat $synthR | grep '^[0-9]' | head -1 | awk '{print $3}')
	abundant_Reads=$(echo "??")
else 
	whitelist_BCs=$(cat $clean10xR | grep "Raw CBs count " | cut -f9 -d' '); 
	subErrorCollapsed_BCs=$(cat $clean10xR | grep "barcodes aggregated" | cut -f2 -d' ');
	abundant_BCs=$(cat $clean10xR | grep "Raw CBs count " | cut -f13 -d' '); 
	abundant_Reads=$(cat $clean10xR | grep "Abundent Reads" | cut -f12 -d' ');
fi

## nof STAMPS 
stampStats=$(grep -v "^#" $dgeR | tail -n +3 | awk -v minUMI="$MIN_UMIS_PER_CELL" '($3>=minUMI){
	stamps=stamps+1; reads=reads+$2; umis=umis+$3; genes=genes+$4
} END {print stamps" "reads/stamps" "umis/stamps" "genes/stamps}')
stamps=$(echo $stampStats | cut -f1 -d' ')
avgReadsPerStamp=$(echo $stampStats | cut -f2 -d' ')
avgUMIsPerStamp=$(echo $stampStats | cut -f3 -d' ')
avgGenesPerStamp=$(echo $stampStats | cut -f4 -d' ')

if [ $PROTOCOL == "dropseq" ]; then
	echo $LIBS_POOL,$libID,,$PROTOCOL,,$rawReadPairs,$QCfilteredReads,,,$unimap,$multimap,$unmap,,$mappedStats,$dupRate,,$geneTagged_BCs,$abundant_BCs,$subErrorCollapsed_BCs,$subErrorDropped_BCs,$synthErrorCollapsed_BCs,$synthErrorDropped_BCs,,,$stamps,,$avgReadsPerStamp,$avgUMIsPerStamp,$avgGenesPerStamp >> $statsFile
else
	echo $LIBS_POOL,$libID,,$PROTOCOL,,$rawReadPairs,$QCfilteredReads,$QCrate,,$unimap,$multimap,$unmap,$mapped_Reads,,$taggedStats,$geneTagged_Rate,$reads_per_UMI,,$whitelist_BCs,$subErrorCollapsed_BCs,$abundant_BCs,$abundant_Reads,,$stamps,$avgReadsPerStamp,$avgUMIsPerStamp,$avgGenesPerStamp >> $statsFile
fi	

head -15 dge.summary.txt | grep -v "[#|_|]" | sed "s/^[ACGT]/$libID\t/" | sed '/^$/d' |  awk '{print $0","$3/$4}' | tr '\t' ',' >> $topBCsFile
done

fct=$(tail -n 1 $statsFile  | tr ',' '\n' | wc -l)
for i in `seq 1 $fct`; do cut -f "$i" -d',' $statsFile  | paste -s; done | tr '\t' ','
cat $topBCsFile


