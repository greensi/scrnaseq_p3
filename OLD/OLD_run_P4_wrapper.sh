#!/bin/bash

## Wrapper script to run raw fastq files from a dropseq,inDrop,or10X experiment through a custom pipeline drawing heavily on:
## 1. the steps described in:
##     Zhang et al 2019, Molecular Cell  doi.org/10.1016/j.molcel.2018.10.020 
## 	and implemented in the python package baseqDrops found at:
##     https://github.com/beiseq/baseqDrops
## 2. the steps described in:
##		Macosko et al 2015, Cell doi.org/10.1016/j.cell.2015.05.002
##	and implemented in the java scripts at:
##      https://github.com/broadinstitute/Drop-seq
## Author: Sharon Greenblum
## Institute: Joint Genome Institute
## Date: June 20, 2019

usage()
{
	echo "usage: run_P4_wrapper.sh configFile [steps=steps] [time=time] [cpus=cpus] [qos=qos]

	** steps must be one or more (comma-separated) of the following keywords:
	
			genomePrep,getfastQ,trimR1,trimR2,align,tag,clean,dge
	
			or 

			all[default]
	
	** note that these scripts may require python 3 [not tested]

	"
}

## set default options
mysteps=all
time=6:00:00
cpus=32
qos=genepool_shared

## set default dirs
currentDir=$PWD
thisScriptDir=`dirname "$0"`
bqDir=$thisScriptDir/my_baseqDrops
dstDir=$thisScriptDir/drop-seq_v2.3

## set paths and tools
bbtools="shifter --image=docker:bryce911/bbtools:latest"
STAR="shifter --image=docker:vrsingan/star:v2.5.4a STAR"
samtools="shifter --image=docker:mjblow/samtools-1.9:latest samtools"

## get variables from command line
config=$1
for i in $(echo "$@" | awk '{$1="";print $0}'); do
case $i in
	steps=*)
    	mysteps="${i#*=}"
    	shift 
    ;;
    trimR1=*)
    	trimR1="${i#*=}"
    	shift 
    ;;
    time=*)
    	time="${i#*=}"
    	shift 
    ;;
    cpus=*)
    	cpus="${i#*=}"
    	shift 
    ;;
    qos=*)
    	qos="${i#*=}"
    	shift 
    ;;
    *)  # unknown option
        echo "unknown argument: $i"; usage; exit 1;  
    ;;
esac
done

## read in config file
. $thisScriptDir/read_P3_config.sh
echo "############################"
echo "# running P4 pipeline for pool [ $LIBS_POOL ]"
echo "# aligning [ $PROTOCOL ] libraries in [ $LIBS_META ]"
echo "# to genomes specified in [ $REF_CONFIG ]"
echo "# and writing files to [ $LIBS_DIR ]
############################"

## set up location of wrapper files
genomePrepScript=$(echo $REF_CONFIG | sed 's/.config$/_dropseqPrep.sh/')
libPrepWrapper=$(echo $config | sed 's/.config$/.p4libPrep.sh/')
echo "#!/bin/bash" > $libPrepWrapper
dependString=""

## parse steps
allSteps=getfastQ,trimR1,trimR2,align,tag,clean,dge
if [ $mysteps == "help" ]; then usage; exit 1; fi
if [ $mysteps == all ]; then 
	mysteps="genomePrep,"$allSteps; 
fi
stepString=""
for step in $(echo $allSteps | tr ',' '\n'); do
	stepString=$stepString$( echo $mysteps | grep -c $step)" "
done

#############
### prep genomes
############
if [ $(echo $mysteps | grep genomePrep) ]; then 
	if [ $REF_OVERWRITE != TRUE ] && [ -e $REF_DIR/$refPrefix/STAR/Genome ]; then
		echo "#using previously created ref genome files in $REF_DIR/$refPrefix
		#set REFGENOME_OVERWRITE to TRUE in config file to overwrite instead"
	else
		if [ ! -e $REF_DIR/$refPrefix ]; then mkdir $REF_DIR/$refPrefix; fi
		#rm -r $REF_DIR/$refPrefix/*
		$dstDir/genomePrep.py $REF_CONFIG > /dev/null  
		mv prep.sh $genomePrepWrapper
		chmod +x $genomePrepWrapper
		echo "
		sID=\$(sbatch --chdir $REF_DIR/$refPrefix -J dropseq.prepGenome_$refPrefix -q genepool_shared -A gtrnd \
		-t 2:00:00 -N 1 -n 1 -c 6 \
		-o" $(echo $genomePrepScript | sed 's/.sh/.out/') "-e" $(echo $genomePrepScript | sed 's/.sh/.err/') $genomePrepScript ")" >> $libPrepWrapper
		dependString="--dependency=afterok:\$sID"
	fi
fi

if [ $mysteps == genomePrep ]; then exit; fi

#############
### prep libraries
############

## get list of libraries from jamo if not supplied
if [ ! -e $LIBS_META ]; then 
	echo "samples in pool are:"
	jamo report \select metadata.library_name where metadata.parent_library_name = $LIBS_POOL,metadata.fastq_type = sdm_normal > $LIBS_META
	cat $LIBS_META
fi

## go through list	
cat $LIBS_META | sed 's/ //g' | sed 's/, /,/g' | tr '\t' ',' | awk -F ',' -v libField=1 '(NR==1){ 
		header=($0 ~ /#/); 
		for(ii=1;ii<=NF;ii++){
			if($ii == "library"){
				libField=ii;header=1
			}
		}
	}
	(NR>1 || !header ){print $libField}' | while read libID; do
	
	## define command file and write sbatch command to libPrepWrapper
	libCmdsFile=$(dirname $config)/$libID.$NAME
	echo "
	sbatch $dependString -J p3.$libID -q $qos -A gtrnd -t $time -N 1 -n 1 \
	$(if [ $qos == "genepool_shared" ]; then echo "-c $cpus"; fi) \
	-o $libCmdsFile.out -e $libCmdsFile.err $libCmdsFile.sh $stepString
	" >> $libPrepWrapper 


	## if commands file already exists, you're done.. move to next lib
	if [ -e $libCmdsFile.sh ]; then echo "$libCmdsFile.sh already exists - delete and re-run to edit"; continue; fi

	## otherwise.....
		
	#set up out directory 
	outdir=$LIBS_DIR/$LIBS_POOL/$libID
	if [ ! -e $outdir/$NAME ]; then mkdir -p $outdir/$NAME; fi

	
	# set up file names
	module unload python
	module load python/2.7-anaconda-4.4
	fastq=$outdir/$libID.fastq.gz
	
	bam=$outdir/$NAME/star_aligned.bam
	funcbam=$outdir/$NAME/function_tagged.bam
	finalbam=$outdir/$NAME/final.bam
	
	subreport=$outdir/$NAME/substitution_error_report.txt
	synthstats=$outdir/$NAME/synthesis_error_stats.txt
	synthsummary=$outdir/$NAME/synthesis_error_summary.txt
	synthreport=$outdir/$NAME/synthesis_error_report.txt
	
	dge=$outdir/$NAME/$libID.dge.gz
	dgesummary=$outdir/$NAME/dge_summary.txt
	dgelong=$outdir/$NAME/$libID.dge_long.gz
	
	#set up commands file
	echo "#!/bin/bash" > $libCmdsFile.sh
	echo "export LC_ALL=C.UTF-8; export LANG=C.UTF-8" >> $libCmdsFile.sh
	echo -e 'doLink=$1; doTrimR1=$2; doTrimR2=$3; doAlign=$4; doTag=$5; doClean=$6; doDGE=$7;' >> $libCmdsFile.sh
	echo "mill_reads=1000;" >> $libCmdsFile.sh
	
	

	## link fastq from jamo
	linkCmd="
	######LinkFastq###########
	if [ \$doLink -gt 0 ]; then
		jamoFile=$(jamo report \select metadata.library_name,file_name \
		where metadata.library_name = $libID,metadata.parent_library_name = $LIBS_POOL,metadata.fastq_type = sdm_normal | \
		awk '{print $2}')
		echo linking $fastq
		cd $outdir; jamo link filename $jamoFile 1>&2 ; mv $jamoFile $(basename $fastq); cd $currentDir

	fi
	"

	## trim
	trimCmd= ""
	########Trim R1#########

	if [ \$doTrimR1 -gt 0 ] && [ $TRIM_R1 -gt 0 ]; then
		if [ ! -e $R1pretrim ]; then 
			if [ ! -e $R1 ]; then echo $R1 not found; exit 1; fi; 
			mv $R1 $R1pretrim; 
		fi; 
		if [ ! -e $R2pretrim ]; then 
			if [ ! -e $R2 ]; then echo $R2 not found; exit 1; fi; 
			mv $R2 $R2pretrim; 
		fi; 
		$cutadapt -u $TRIM_R1 \
-m 25 \
-o $R1 \
-p $R2 \
$R1pretrim \
$R2pretrim
	fi
	" >> $libCmdsFile.sh

	## trim R2
	## find at least 5 contiguous bases of SMART adapter from 5' end with no errors, or full adapter sequence from anywhere with up to 2 errors, hardclip all bp left of match
	## trim 5' bases with quality <10
	## remove reads with trimmed length <20

	SEQ=AAGCAGTGGTATCAACGCAGAGTGAATGGG
	SEQlen=$(echo $SEQ | wc -c)

echo "
$bbtools bbduk.sh in=$fastq ktrim=l k=$SEQlen hdist=2 mink=5 hdist2=0 \
literal=AAGCAGTGGTATCAACGCAGAGTGAATGGG \
rcomp=f qtrim=l trimq=10 minlength=20 out=stdout.fq int=f | shifter --image=docker:bryce911/bbtools:latest bbduk.sh in=stdin.fq ktrim=r k=6 hdist=0 literal=AAAAAA rcomp=f qtrim=r trimq=10 minlength=10 out=stdout.fq int=f	## find at least 6 contiguous As, no errors hard clip all bases 3'
" >> $libCmdsFile.sh

	### map fastq and add cell and UMI tags
echo "
########Align R2#########
if [ \$doAlign -gt 0 ]; then
	${STAR} \
--genomeDir $REF_DIR/$refPrefix/STAR \
--readFilesIn <(python $thisScriptDir/run_p4_wrapper.tag_fastq.py $PROTOCOL $R1 $R2 /dev/stdout) \
--runThreadN $cpus \
--outFileNamePrefix $outdir/$NAME/star \
--outStd BAM_Unsorted --outSAMtype BAM Unsorted | \
$samtools view -h -F 260 | \
awk -F '_' '(\$1 ~ /^@/){print}(\$1 !~ /^@/){print \$0\"\tXC:Z:\"\$2\"\tXM:Z:\"\$3}' | \
$samtools sort -o - -O bam -@ $cpus - | \
$dstDir/TagReadWithInterval I=/dev/stdin O=$bam TMP_DIR=$outdir INTERVALS=$REF_DIR/$refPrefix/$refPrefix.genes.intervals TAG=XG;
fi
" >> $libCmdsFile.sh


	## tag reads with gene using dropseq tools
	echo "
	########Gene Tag#########
	if [ \$doTag -gt 0 ]; then
		$dstDir/TagReadWithGeneFunction O=$funcbam ANNOTATIONS_FILE=$REF_DIR/$refPrefix/$refPrefix.refFlat INPUT=$bam; 
	fi
	" >> $libCmdsFile.sh

	## do bead cleanup
	case $PROTOCOL in
	"dropseq" ) 
		echo "
		if [ \$doClean -gt 0 ]; then
			$dstDir/DetectBeadSubstitutionErrors INPUT=$funcbam OUTPUT=$subbam TMP_DIR=$outdir MIN_UMIS_PER_CELL=20 OUTPUT_REPORT=$subreport; 
			$dstDir/DetectBeadSynthesisErrors INPUT=$subbam MIN_UMIS_PER_CELL=20 OUTPUT_STATS=$synthstats SUMMARY=$synthsummary REPORT=$synthreport CREATE_INDEX=true TMP_DIR=$outdir OUTPUT=$finalbam; 
		fi
		" >> $libCmdsFile.sh
		;;
	"10X" )	  
		echo "
		if [ \$doClean -gt 0 ]; then
			echo barcode,counts > $bccounts; 
			python $thisScriptDir/run_p4_wrapper.clean_10x.py $bam $PROTOCOL $bccounts 20 $bcstats $HAMMING $WHITELIST
			cat $bcstats | awk -F ',' '{bcCorrect=$1;split($4,bcMM,\",\");print(bcCorrect\",\"bcCorrect);for(bc in bcMM){print bcMM\",\"bcCorrect}}' > $bclist
			samtools view -H $funcbam > $funcbam.header 
			samtools view $funcbam | grep -f <(cat $bclist | cut -f1 -d',' | sed 's/^/XC:Z:/') | awk ' \
			BEGIN{ while((getline line<\"$bclist\") > 0 ) {split(line,fields,\",\");bc[\"XC:Z:\"fields[1]]=\"XC:Z:\"fields[2]} }; \
			{for(ii=1;ii<=NF;ii++){if(\$ii ~ /XC:Z:/){\$ii=bc[\$ii];print \$0;break}}}' | cat $funcbam.header  - | $samtools view -Sb - > $finalbam
		fi
		" >> $libCmdsFile.sh
		;;
	esac

	## make DGE
	echo "
	if [ \$doDGE -gt 0 ]; then
		$dstDir/DigitalExpression I=$finalbam 	O=$dge 	SUMMARY=$dgesummary MIN_NUM_TRANSCRIPTS_PER_CELL=$MIN_UMIS_PER_CELL OUTPUT_LONG_FORMAT=$dgelong
		rm $bam $genebam $funcbam $subbam 
	fi
	" >> $libCmdsFile.sh


	chmod +x $libCmdsFile.sh

done

chmod +x $libPrepWrapper
echo "
# prep libraries by running
$libPrepWrapper
"


