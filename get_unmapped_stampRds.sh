
sampDir=$1
samp=basename($sampDir)
cd $sampDir

#make list of valid cell bcs
cat p4/dge.summary.txt | grep "^[CATG][CATG]" | cut -f1 | > p4/seurat.bcList.txt

#make list of mapped read names from valid cell bcs
zcat p4/tag.cellUMICts.gz | grep -f p4/seurat.bcList.txt | awk '{
	cell=$1;split($3,UMIlist,":");
	for(UMI in UMIlist){print "_"cell"_"UMIlist[UMI]}
}' > p4/seurat.bcList_UMIs.txt

#read in raw fastq
#grep for valid cell bcs at start of R1 sequence with -B 1 -A 6 to get both fwd and rev reads
#pipe to tag step but without index/counter
#pipe to bbtools to remove mapped read names

zcat $samp.fastq.gz | grep -B 1 -A 6 -f <(cat p4/seurat.bcList.txt| sed 's/^/^/') | \
grep -v "^--" | python $HOME/scripts/scRNAseq_P3/tag_fastq.py -p 10X -o /dev/stdout -f /dev/stdin | \
sed 's/_[0-9].*//' | \
shifter --image=docker:bryce911/bbtools:latest filterbyname.sh \
in=stdin.fq names=p4/seurat.bcList_UMIs.txt int=f out=$samp.stampReads_unmapped.fastq.gz 