## Overview
+ This is a pipeline to analyze raw fastq files from a single-cell RNAseq run, and produce a digital expression matrix (DGE) of UMI counts for each gene (rows) in each cell (columns).
+ The final DGE matrix can then be used directly as an input to downstream analysis tools like Seurat or Pagoda.
+ The pipeline is capable of analyzing data from either drop-seq-like or 10x Chromium protocols.
+ A number of log files are produced to assess data quality at each step.
+ The final step of the pipeline creates a .json file so that the results can be submitted to jamo.
+ A single script is created for each sample that includes all steps of the pipeline. These scripts can be run locally, or submitted to the compute cluster via an automatically-generated wrapper script. 

## Source files  
bitbucket.org/greensi/scrnaseq_p3/  

## Dependencies
+ python3
+ python packages:
	+ pandas
	+ numpy
	+ time
	+ pysam
+ gtftogenepred
	+ can be installed via conda with: conda install -c bioconda ucsc-gtftogenepred
+ drop-seq-tools (https://github.com/broadinstitute/Drop-seq/releases)
	+ drop-seq_v2.3 is included in this repository and will be used automatically, but newer versions can be downloaded and the path can be supplied as an option at runtime
	
	
## Shifter images used
+ bryce911/bbtools
+ dmccloskey/cufflinks
+ mjblow/samtools-1.9:latest
+ stackleader/bgzip-utility:latest
+ mgibio/star (default; other versions can be supplied on command line at runtime)

## Step 1: Prepare reference genome files
Before analyzing any raw data, a directory of indexed reference files for the appropriate species (single species or multi-species) must be created, using the script 'prepare_ref_data.sh'
The pipeline depends on the RQC reference database (https://rqc.jgi-psf.org/seq_jat_import/page/search) for raw genome fastas and annotation (gff3) files.  The first step is to find the 'JAT-key' (RQC and jamo identifier) associated with the desired species' annotation file(s).
A script is included in this repository to query the RQC database and return possible entries based on a search term.  (This search can also be done on the RQC webpage)

For example:  
```get_RQC_annotation_jatkeys.sh Ara```  
returns  
```
AUTO-152363,Arabidopsis thaliana,/global/dna/dm_archive/rqc/analyses/AUTO-152363,Athaliana_167_TAIR10.gene_exons.gff3,[u'AUTO-152361', u'AUTO-152362']
AUTO-237994,Arabidopsis lyrata,/global/dna/dm_archive/rqc/analyses/AUTO-237994,Alyrata_384_v2.1.gene_exons.gff3,[u'AUTO-237992', u'AUTO-237993']
AUTO-243756,Arabidopsis thaliana var. Col-0,/global/dna/dm_archive/rqc/analyses/AUTO-243756,GCF_000001735.4_TAIR10.1_genomic.gff,[u'AUTO-243754']
AUTO-272571,Arabidopsis thaliana,/global/dna/dm_archive/rqc/analyses/AUTO-272571,Athaliana_447_Araport11.gene_exons.gff3,[u'AUTO-138486']
```
If the the last line contained the annotation file I wanted to use, I would then run:  
```prepare_ref_data.sh AUTO-272571 <my-ref-dir>```  
where <my-ref-dir> is the location I want the new reference directory to be created in.  


The output of this script is a single line you can copy and paste to submit the job to the cluster. Example output from running the command above:  
```
finding annotation file associated with AUTO-272571
finding genome file associated with AUTO-272571
ref genome prep config file written to refData/AUTO-272571/scRNA_ref_prep.config

# prep genome by running
sbatch --chdir refData/AUTO-272571 -J prep_genomes.AUTO-272571 -q genepool_shared -A gtrnd -t 2:00:00 -N 1 -n 1 -c 6 -o refData/AUTO-272571/scRNA_ref_prep.out -e refData/AUTO-272571/scRNA_ref_prep.err refData/AUTO-272571/scRNA_ref_prep.sh 

``` 

## Step 2: Create a config file
A config file listing data parameters is required to run the analysis pipeline. The config file should be a simple text file defining variables, and will be sourced at runtime. 
The file can be named whatever you'd like.  
### required parameters are:
+      	PROTOCOL : (10X|dropseq)
+		FASTQ_DIR : fastq files should be [linked to]/[in] $FASTQ_DIR
+		LIBS_LIST : filename of the list of library IDs to analyze; can be one of the following scenarios:  
	+ file exists and contains a single column list of library IDs with no header --> files in FASTQ_DIR named <libraryID\>.fastq.gz will be analyzed
	+ file exists and contains a tab-separated table of metadata with 'library' as the relevant column header --> same as above
	+ file does not exist yet, and POOL_ID is supplied --> file will be created listing all matching RNA libraries in jamo
	+ file does not exist yet, and POOL_ID is not supplied --> file will be created listing all fastq files in FASTQ_DIR
+		REF_DIR : directory with prepped genome files (see step 1 above)
+		OUT_DIR : parent directory of output files 
### optional parameters are:
+		POOL_ID : pool ID in jamo, not needed if fastq files have already been downloaded/linked (default none); 
+		INTERLEAVED : whether or not fastq files are interleaved; if FALSE, expects file names to contain the strings 'R1' and 'R2' (default TRUE)
+		TRIM_R1 : how many leading bp of R1 to trim; can be any value from 0-130 (default: 0)
+		WHITELIST : filter barcodes to those in this file (default: none; 10X only)
+		HAMMING(TRUE|FALSE) : aggregate barcodes within 1 hamming distance (default: TRUE; 10X only)
+		MIN_UMIS_PER_CELL : minimum nof UMIs per barcode to include in the DGE (default: 1000)

### example config file: 
*(note that in the example below, NAME is not a parameter used by the pipeline, but will be substituted into other parameters accordingly as the config file is sourced)*  

PROTOCOL=dropseq  
NAME=GOBXN_ds_At  
POOL_ID=GOBXN  
FASTQ_DIR=$HOME/seqData/scRNA/${NAME}  
LIBS_LIST=$HOME/configs/scRNA/${NAME}/libs.list  
REF_DIR=$SCRATCH/refData/AUTO-272571  
OUT_DIR=$SCRATCH/analysis/scRNA/${NAME}  
TRIM_R1=25  
MIN_UMIS_PER_CELL=1000  


## Step 3: Run analysis  

The analysis pipeline consists of 6 steps.  

+ **link**: find raw fastq files for library IDs in jamo; create links to them in FAST_DIR; requires POOL_ID
+ **trim**: trim leading basepairs from R1 (if specified), trim adapters and poly-A tails from R2; writes a trimmed fastq file
+ **align**: align trimmed fastq to reference genome iwth STAR, tag and keep uniquely-mapping reads aligning to gene regions  
+ **clean**:  
	+	(PROTOCOL=dropseq) fix barcode errors with drop-seq-tools DetectBeadSubstitutionErrors and DetectBeadSynthesisErrors  
	+	(PROTOCOL=10X) custom scripts used to filter to barcodes on whitelist with at least 20 UMIs, then find + fix non-whitelist barcodes unambiguously 1 edit-distance away from a filtered barcode  
+ **dge**: create a digital expression matrix for 'cleaned' cell barcodes with at least MIN_UMIS_PER_CELL UMIs
+ **archive**: create a .json file for submission to jamo (won't actually submit though)

Default usage is to run all steps for every sample. (parameters in brackets are optional and show defaults)  
```run_analysis.sh <config file> [steps=all] [time=6:00:00] [cpus=32] [qos=genepool_shared]```

You can also choose to run any subset of steps by using a comma-separated list.  
```run_analysis.sh <config file> steps=align,clean,dge,archive```  

Notes:  
+ time and cpus refer to the allocations for _each_ sample for _all_ specified steps  
+ the wrapper script will write a shell script for each sample, as well as a wrapper file for all samples with cluster configuration parameters  
+ at the end, copy/paste the name of the supplied executable wrapper file to submit the jobs  

## References
### This pipeline draws heavily on:
1. the steps described in:  
_Zhang et al 2019, Molecular Cell  doi.org/10.1016/j.molcel.2018.10.020_  
and implemented in the python package baseqDrops found at:  
_https://github.com/beiseq/baseqDrops_  
2. the steps described in:  
_Macosko et al 2015, Cell doi.org/10.1016/j.cell.2015.05.002_  
and implemented in the java scripts at:  
_https://github.com/broadinstitute/Drop-seq_  


#### Author: Sharon Greenblum
#### Institute: Joint Genome Institute Date: June 20, 2019

